import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

//Screens
import BottomNavBar from '../components/layers/BottomNavBar';
import { ActiveScreenProvider } from '../context/ScreenContext';
import AppNavigator from '../components/layers/AppNavigator';

const Router = () => {
  return (
    <NavigationContainer>
      <ActiveScreenProvider>
       {/* <BottomNavBar /> */}
       <AppNavigator />
      </ActiveScreenProvider>
    </NavigationContainer>
  );
}

export default Router;