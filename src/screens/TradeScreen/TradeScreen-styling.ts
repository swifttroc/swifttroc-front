import styled from 'styled-components/native';
/* not sure I'll use this style cuz' this screen is complex and special */
export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #cabcbc;
  padding: 20px;
`;

export const Title = styled.Text`
  font-size: 24px;
  font-weight: bold;
  color: #333;
  text-align: center;
  margin-bottom: 20px;
`;

export const Section = styled.View`
  margin-bottom: 20px;
`;

export const SectionTitle = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #555;
  margin-bottom: 10px;
`;

export const ItemsRow = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: #4b4ced;
  padding: 20px;
  border-radius: 10px;
`;

export const ItemsRow2 = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: #00a1ea;
  padding: 20px;
  border-radius: 10px;
`;

export const ImageContainer = styled.View`
  width: 50px;
  height: 50px;
  margin-right: 10px;
`;

export const Image = styled.Image`
  width: 100%;
  height: 100%;
  border-radius: 5px;
`;

export const Spacer = styled.View`
  flex: 1;
`;

export const ProposalContainer = styled.View`
  background-color: #d3d3d3;
  padding: 20px;
  margin-bottom: 20px;
  border-radius: 10px;
  height: 150px;
`;

export const ProposalContainerDark = styled.View`
  background-color: #a9a9a9;
  padding: 20px;
  margin-bottom: 20px;
  border-radius: 10px;
  height: 150px;
`;

export const ProposalTitle = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: #333;
`;

export const SendTradeButton = styled.TouchableOpacity`
  background-color: #4b4ced;
  padding: 10px;
  border-radius: 5px;
  align-items: center;
`;

export const SendTradeButtonText = styled.Text`
  color: #fff;
`;
