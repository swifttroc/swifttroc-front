import React, { useState, useEffect, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { Image, StyleSheet, View } from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Gesture, GestureDetector, ScrollView } from 'react-native-gesture-handler';
import Animated, { runOnJS, useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated';
import { ObjectsAPI } from '../../API/ObjectsAPI';
import { Text } from 'react-native-svg';


// let myImages = [];
const dragImages = [
    { id: '1', path: 'https://via.placeholder.com/100.png?text=Image+1' },
    { id: '2', path: 'https://via.placeholder.com/100.png?text=Image+2' },

];

const TradeScreen = ({route}) => {
  const { OwnerId } = route?.params || {};
  console.log('OWNER ID:', OwnerId);
    // const [draggedImages, setDraggedImages] = useState(myImages);
    const [objects, setObjects] = useState([]);
    const [draggedImages, setDraggedImages] = useState([]);
    useFocusEffect(
      useCallback(() => {
          // setActiveMenu("");
          getAuthObjects();
      }, [])
  );

    const getAuthObjects = async () => {
      ObjectsAPI.getMyObjects()
          .then((response) => {
              console.log('MY OBJECTS', response.data);
              setObjects(response.data);
          })
          .catch((error) => {
              console.log(error);
          });
  };

    const translationValueX = draggedImages.map(() => useSharedValue(0));
    const translationValueY = draggedImages.map(() => useSharedValue(0));

    const [dropzoneLayout, setDropzoneLayout] = useState({
        x: 0, y: 0, width: 0, height: 0
    });
    const [dragLayout, setDragLayout] = useState({
        x: 0, y: 0, width: 0, height: 0
    });
    const [imageLayout, setImageLayout] = useState([]);

    const createPanGestureHandler = (index) => {
        return Gesture.Pan()
            .onUpdate((event) => {
                translationValueX[index].value = event.translationX;
                translationValueY[index].value = event.translationY;
            })
            .onEnd((event) => {
                const dropX = dragLayout.width + dropzoneLayout.x;
                const dropY = dropzoneLayout.y;

                if (imageLayout[index] &&
                    imageLayout[index].x + event.translationX >= dropX &&
                    imageLayout[index].x + event.translationX + imageLayout[index].width <= dropX + dropzoneLayout.width &&
                    imageLayout[index].y + event.translationY >= dropY &&
                    imageLayout[index].y + event.translationY + imageLayout[index].height <= dropY + dropzoneLayout.height
                ) {
                    runOnJS(dropSuccess)(index);
                } else {
                    translationValueX[index].value = withSpring(0);
                    translationValueY[index].value = withSpring(0);
                }
            });
    };

    const dropSuccess = (index) => {
        console.log("DROPPED SUCCESS", index);
    };

    const panGestureHandler = draggedImages.map((_, index) => createPanGestureHandler(index));

    const animatedStyles = (index) => useAnimatedStyle(() => ({
        transform: [
            { translateX: translationValueX[index].value },
            { translateY: translationValueY[index].value },
        ],
    }));

    return (
        <View style={styles.container}>
            <View style={styles.mainView}>
                 <View
                    contentContainerStyle={styles.dragViewContentContainer}
                    showsVerticalScrollIndicator={true}
                    centerContent={true}
                    // style={styles.dragView}
                    onLayout={(event) => {
                      const { width, x, y, height } = event.nativeEvent.layout;
                      setDragLayout({ width, x, y, height });
                    }}
                >
                    {/* {objects?.map((item, index) => {
    console.log(`Rendering item with id: ${item.id}`);
    return (
        <GestureDetector key={item.id} gesture={panGestureHandler[index]}>
            <Animated.View
                onLayout={(event) => {
                    const { width, x, y, height } = event.nativeEvent.layout;
                    const updatedLayout = [...imageLayout];
                    updatedLayout[index] = { width, x, y, height };
                    setImageLayout(updatedLayout);
                }}
                style={animatedStyles(index)}
            >
                <Image
                    source={{ uri: item.uri }}
                    style={styles.imageView}
                />
            </Animated.View>
        </GestureDetector>
    );
})} */}

                </View>
                <View style={styles.dropView}>
                    <View
                        onLayout={(event) => {
                            const { width, x, y, height } = event.nativeEvent.layout;
                            setDropzoneLayout({ width, x, y, height });
                        }}
                        style={styles.dropZone}
                    />
                </View>
                
                <View style={styles.dragView2}>
                
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
  mainView: {
      flex: 1,
      flexDirection: 'row',
  },
  dragView: {
      height: hp(100),
      width: wp(20),
      borderWidth: 2,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#0621ab',
  },
  dragView2: {
    height: hp(100),
    width: wp(20),
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ab4306',
},
  dropView: {
      height: hp(100),
      width: wp(50),
      borderWidth: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ababab',
      zIndex:-100
  },
  imageView: {
      width: 100,
      height: 100,
      resizeMode: 'contain',
      borderWidth: 1,
      marginVertical: 10,
      zIndex:9999
  },
  dropZone: {
      width: wp(50),
      height: hp(95),
      borderWidth: 1,
      backgroundColor: '#1ce7ee',
      zIndex: -1000,
  },
  dragViewContentContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: '#03c65b',
    zIndex: 9999,
},
});

export default TradeScreen;



// import React, { useState } from 'react';
// import { Image, StyleSheet, Touchable, TouchableOpacity, View } from 'react-native';
// import {
//     heightPercentageToDP as hp,
//     widthPercentageToDP as wp,
// } from 'react-native-responsive-screen';
// import { Gesture, GestureDetector, ScrollView } from 'react-native-gesture-handler';
// import Animated, { runOnJS, useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated';
// import { ObjectsAPI } from '../../API/ObjectsAPI';


// // Replace with your own fake data
// //I want the fake data to be replaced by the data fetched from the API
// let myImages = [];
// const fetchMyObjects = async () => {

//     try {
//         const response = await ObjectsAPI.getMyObjects();
//         // console.log('API response:', response);
//         const myObjects = response.data;
//         if (Array.isArray(myObjects)) {
//             myObjects.forEach((obj) => {
//                 myImages?.push({
//                     id: obj._id,
//                     uri: obj.images[0] || 'https://via.placeholder.com/50x50.png?text=Objet+20',
//                     title: obj.title || 'No Title',
//                 });
//             });

//             console.log('Combined Images Array:', myImages);


//         } else {
//             console.error('Error: Expected an array but got:', myObjects);
//         }
//     } catch (error) {
//         console.error('Error fetching my objects:', error);
//     }
// };
// fetchMyObjects();
// const dragImages = [
//     { id: '1', path: 'https://via.placeholder.com/100.png?text=Image+1' },
//     { id: '2', path: 'https://via.placeholder.com/100.png?text=Image+2' },

// ];

// const TradeScreen = ({ route }) => {
//     const { OwnerId } = route?.params || {};
//     const [draggedImages, setDraggedImages] = useState(myImages);
//     const translationValueX = draggedImages.map(() => useSharedValue(0));
//     const translationValueY = draggedImages.map(() => useSharedValue(0));

//     const [dropzoneLayout, setDropzoneLayout] = useState({
//         x: 0, y: 0, width: 0, height: 0
//     });
//     const [dragLayout, setDragLayout] = useState({
//         x: 0, y: 0, width: 0, height: 0
//     });
//     const [imageLayout, setImageLayout] = useState([]);

//     const createPanGestureHandler = (index) => {
//         return Gesture.Pan()
//             .onUpdate((event) => {
//                 translationValueX[index].value = event.translationX;
//                 translationValueY[index].value = event.translationY;
//             })
//             .onEnd((event) => {
//                 const dropX = dragLayout.width + dropzoneLayout.x;
//                 const dropY = dropzoneLayout.y;

//                 if (imageLayout[index] &&
//                     imageLayout[index].x + event.translationX >= dropX &&
//                     imageLayout[index].x + event.translationX + imageLayout[index].width <= dropX + dropzoneLayout.width &&
//                     imageLayout[index].y + event.translationY >= dropY &&
//                     imageLayout[index].y + event.translationY + imageLayout[index].height <= dropY + dropzoneLayout.height
//                 ) {
//                     runOnJS(dropSuccess)(index);
//                 } else {
//                     translationValueX[index].value = withSpring(0);
//                     translationValueY[index].value = withSpring(0);
//                 }
//             });
//     };

//     const dropSuccess = (index) => {
//         console.log("DROPPED SUCCESS", index);
//     };

//     const panGestureHandler = draggedImages.map((_, index) => createPanGestureHandler(index));

//     const animatedStyles = (index) => useAnimatedStyle(() => ({
//         transform: [
//             { translateX: translationValueX[index].value },
//             { translateY: translationValueY[index].value },
//         ],
//     }));

//     return (

//         <View style={styles.mainView}>
//             <View style={styles.dragView0}>
//                 <View
//                     contentContainerStyle={styles.dragViewContentContainer}
//                     // showsVerticalScrollIndicator={true}
//                     centerContent={true}
//                     // style={styles.dragView}
//                     onLayout={(event) => {
//                         const { width, x, y, height } = event.nativeEvent.layout;
//                         setDragLayout({ width, x, y, height });
//                     }}
//                 >
//                     {draggedImages.map((item, index) => (
//                         <GestureDetector key={item.id} gesture={panGestureHandler[index]}>
//                             <Animated.View
//                                 onLayout={(event) => {
//                                     const { width, x, y, height } = event.nativeEvent.layout;
//                                     const updatedLayout = [...imageLayout];
//                                     updatedLayout[index] = { width, x, y, height };
//                                     setImageLayout(updatedLayout);
//                                 }}
//                                 style={animatedStyles(index)}
//                             >
//                                 <Image
//                                     source={{ uri: item.uri }}
//                                     style={styles.imageView}
//                                 />

//                             </Animated.View>
//                         </GestureDetector>
//                     ))}
//                 </View>

//             </View>

//             <View style={styles.dropView}>
//                 <View
//                     onLayout={(event) => {
//                         const { width, x, y, height } = event.nativeEvent.layout;
//                         setDropzoneLayout({ width, x, y, height });
//                     }}
//                     style={styles.dropZone}
//                 />

//             </View>

//             {/* <Image source={{ uri: 'https://picsum.photos/id/237/200/300' }} style={styles.imageView} /> */}


//             <View style={styles.dragView2}>
//                 <Image source={{ uri: 'https://picsum.photos/id/237/200/300' }} style={styles.imageView} />
//                 <Image source={{ uri: 'https://picsum.photos/id/237/200/300' }} style={styles.imageView} />
//                 <Image source={{ uri: 'https://picsum.photos/id/237/200/300' }} style={styles.imageView} />
//                 <Image source={{ uri: 'https://picsum.photos/id/237/200/300' }} style={styles.imageView} />
//             </View>

//         </View>


//     );
// };

// const styles = StyleSheet.create({

//     mainView: {
//         flex: 1,
//         flexDirection: 'row',
//         // backgroundColor: '#3d0202',
//         // borderWidth: 2,
//         // height: hp(80),
//         // width: wp(25),
//     },
//     dragView: {
//         height: hp(80),
//         width: wp(20),
//         borderWidth: 3,
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: '#ab0687',
//     },
//     dragView0: {
//         height: hp(80),
//         width: wp(20),
//         borderWidth: 3,
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: '#2f5f3e',
//     },
//     dragView2: {
//         height: hp(80),
//         width: wp(20),
//         borderWidth: 2,
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: '#06ab74',
//     },
//     dropView: {
//         height: hp(80),
//         width: wp(50),
//         borderWidth: 1,
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: '#2a3329',
//         zIndex: -100
//     },
//     imageView: {
//         width: 100,
//         height: 100,
//         resizeMode: 'contain',
//         borderWidth: 1,
//         marginVertical: 10,
//         zIndex: 9999
//     },
//     dropZone: {
//         width: wp(80),
//         height: hp(80),
//         borderWidth: 1,
//         backgroundColor: '#1ce7ee',
//         zIndex: -1000,
//     },
//     dragViewContentContainer: {
//         alignItems: 'center',
//         justifyContent: 'center',
//         flexDirection: 'column',
//         backgroundColor: '#c6ac03',
//         zIndex: 9999,
//     },
// });

// export default TradeScreen;
