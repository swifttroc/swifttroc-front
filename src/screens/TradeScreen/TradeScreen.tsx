import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Background from '../../components/layers/Background';
import { ObjectsAPI } from '../../API/ObjectsAPI';
import { UsersAPI } from '../../API/UsersAPI';
import ChevronLeftIcon from '../../components/icons/ChevronLeftIcon';
import { useNavigation } from '@react-navigation/native';
import { TradesAPI } from '../../API/TradesAPI';
import Toast from 'react-native-toast-message';


const TradeScreen: React.FC = ({ route }: {route?: any}) => {

  const navigation = useNavigation();
  const { OwnerId, trade, type } = route.params || '';

  const [owner, setOwner] = useState<any>();
  const [myObjects, setMyObjects] = useState(Array<any>());
  const [hisObjects, setHisObjects] = useState(Array<any>());
  const [proposedObjects, setProposedObjects] = useState(Array<any>());
  const [receipedObjects, setReceipedObjects] = useState(Array<any>());

  useEffect(() => {
    fetchOwner();
    fetchMyObjects();
    fetchHisObjects();

    if(trade) {
      console.log('###TRADE###:', type);
      console.log('MyOBJECTSLENGHT', myObjects.length);
      console.log('HisOBJECTSLENGHT', hisObjects.length);
      if(type === 'received') {
        setProposedObjects(trade.recipientItems);
        setReceipedObjects(trade.proposerItems);
        // setMyObjects(myObjects?.filter((object) => !trade.recipientItems.includes(object._id)));
        // setHisObjects(hisObjects?.filter((object) => !trade.proposerItems.includes(object._id)));
      }
      else {
        setProposedObjects(trade.proposerItems);
        setReceipedObjects(trade.recipientItems);
        // setMyObjects(myObjects?.filter((object) => !trade.proposerItems.includes(object._id)));
        // setHisObjects(hisObjects?.filter((object) => !trade.recipientItems.includes(object._id)));
      }
    }
  }, [route.params]);

  const fetchOwner = async () => {
    try {
      let ID;
      if(trade && type === 'received') {ID = trade.proposer?._id;}
      else if(trade && type === 'sent') {ID = trade.recipient?._id;}
      else {ID = OwnerId;}
      const response = await UsersAPI.getuserById(ID);
      if (response?.data) {
          setOwner(response.data);
      } else {
          console.log("Error fetching red data:", response?.error || "Unknown error");
      }
      
    } catch (error) {
        console.log("Error fetching red data:", error);
    }
  };

  const fetchMyObjects = async () => {
    try {
      const response = await ObjectsAPI.getMyObjects();
      if (response?.data) {
          console.log("My objects fetched:", response.data.length);
          setMyObjects(response.data);
      } else {
          console.log("Error fetching red data:", response?.error || "Unknown error");
      }
    } catch (error) {
        console.error("Error fetching red data:", error);
    }
  };

  const fetchHisObjects = async () => {
    try {
      let ID;
      if(trade && type === 'received') {ID = trade.proposer?._id;}
      else if(trade && type === 'sent') {ID = trade.recipient?._id;}
      else {ID = OwnerId;}
      const response = await ObjectsAPI.getSpecificUserObjects(ID);
      if (response?.data) {
          console.log("His objects fetched:", response.data.length);
          setHisObjects(response.data);
      } else {
          console.log("Error fetching red data:", response?.error || "Unknown error");
      }
    }
    catch (error) {
        console.error("Error fetching red data:", error);
    }
  };

  const validationHandler = () => {

    const proposedObjectsIds = proposedObjects.map((object) => object._id);
    const receipedObjectsIds = receipedObjects.map((object) => object._id);


    const trade = {
      recipient: OwnerId,
      proposerItems: proposedObjectsIds,
      recipientItems: receipedObjectsIds,
    };

    console.log('Trade to send:', trade);

    // Send the trade proposal
    TradesAPI.POST(trade)
    .then((response) => {
      if(!response?.status || response?.status != 201) throw (response?.data?.message || 'Erreur lors de l\'envoi de la proposition de troc');
      Toast.show({
        type: 'success',
        text1: 'Proposition envoyée',
        text2: 'La proposition de troc a bien été envoyée.',
      });
      navigation.goBack();
    })
    .catch((error) => {
      Toast.show({
        type: 'error',
        text1: 'Erreur',
        text2: error,
      });
    });

  };

  const addObjectToProposeItems = (object: any) => {
    //TODO: enlever pour contre proposition
    if(trade) return;
    console.log('Object added to propose items:', object);
    //push the object to the proposed objects array
    setProposedObjects([...proposedObjects, object]);

    //remove from myObjects
    setMyObjects(myObjects?.filter((item) => item._id !== object._id));

  };

  const addToReceipedItems = (object: any) => {
    //TODO: enlever pour contre proposition
    if(trade) return;
    console.log('Object added to receiped items:', object);
    //push the objectID to the proposed objects array
    setReceipedObjects([...receipedObjects, object]);

    //remove from hisObjects
    setHisObjects(hisObjects?.filter((item) => item._id !== object._id));
  };

  const removeProposedObject = (object: any) => {
    //TODO: enlever pour contre proposition
    if(trade) return;
    console.log('Object removed from proposed items:', object);
    //remove from proposedObjects
    setProposedObjects(proposedObjects?.filter((item) => item._id !== object._id));

    //add to myObjects
    setMyObjects([...myObjects, object]);
  };

  const removeReceipedObject = (object: any) => {
    //TODO: enlever pour contre proposition
    if(trade) return;
    console.log('Object removed from receiped items:', object);
    //remove from receipedObjects
    setReceipedObjects(receipedObjects?.filter((item) => item._id !== object._id));

    //add to hisObjects
    setHisObjects([...hisObjects, object]);
  };

  const acceptProposal = () => {
    console.log('Accepting proposal');
    try{
      TradesAPI.PUT(trade._id, 'accepted')
      .then((response) => {
        if(!response?.status || response?.status != 200) throw (response?.data?.message || 'Erreur lors de l\'acceptation de la proposition de troc');
        Toast.show({
          type: 'success',
          text1: 'Proposition acceptée',
          text2: 'La proposition de troc a bien été acceptée.',
        });
        navigation.goBack();
      })
      .catch((error) => {
        Toast.show({
          type: 'error',
          text1: 'Erreur',
          text2: error,
        });
      });
    } catch (error: any) {
      Toast.show({
        type: 'error',
        text1: 'Erreur',
        text2: error,
      });
    }
  };
  const refuseProposal = () => {
    console.log('Refusing proposal');
    try{
      TradesAPI.PUT(trade._id, 'rejected')
      .then((response) => {
        if(!response?.status || response?.status != 200) throw (response?.data?.message || 'Erreur lors de la réfus de la proposition de troc');
        Toast.show({
          type: 'success',
          text1: 'Proposition refusée',
          text2: 'La proposition de troc a bien été refusée.',
        });
        navigation.goBack();
      })
      .catch((error) => {
        Toast.show({
          type: 'error',
          text1: 'Erreur',
          text2: error,
        });
      });
    } catch (error: any) {
      Toast.show({
        type: 'error',
        text1: 'Erreur',
        text2: error,
      });
    }
  };

  return (
    <Background>
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={{}}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginBottom: 20,
        }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <ChevronLeftIcon size={30} color='white' />
          </TouchableOpacity>
          <Text style={styles.title}>Interface d'échange</Text>
          <View />
        </View>

        {/* Top section for "Mes objets" */}
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>Mes objets</Text>
          <View style={styles.itemsRow}>
            <ScrollView horizontal>
              {myObjects?.length >0 && myObjects?.map((object) => (
                <TouchableOpacity key={object._id} onPress={() => addObjectToProposeItems(object)} style={[styles.imageContainer]}>
                <Image source={{ uri: object.images[0] }} style={styles.image} />
                <Text>{object.title}</Text>
              </TouchableOpacity>
              ))}
            </ScrollView>  
          </View>
        </View>

        <View
          style={styles.proposalContainer}
        >
          <Text style={styles.proposalTitle}>Ce que j'échange</Text>
          <ScrollView horizontal contentContainerStyle={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            {proposedObjects?.length > 0 && proposedObjects?.map((object: any) => (
              <TouchableOpacity onPress={()=> removeProposedObject(object)} key={object._id} style={[styles.imageContainer]}>
                <Image source={{ uri: object.images[0] }} style={styles.image} />
                <Text>{object.title}</Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>


        {/* Dark gray rectangle: "Ce que Thomas propose" */}
        <View
          style={styles.proposalContainerDark}
        >
            <Text style={styles.proposalTitle}>Ce que je reçoit de {owner?.pseudo}</Text>
          <ScrollView horizontal contentContainerStyle={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            {receipedObjects?.length > 0 && receipedObjects?.map((object: any) => (
              <TouchableOpacity onPress={() => removeReceipedObject(object)} key={object._id} style={[styles.imageContainer]}>
                <Image source={{ uri: object.images[0] }} style={styles.image} />
                <Text>{object.title}</Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>

        {/* Bottom section for "Les objets de Thomas" */}
        
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>Les objets de {owner?.pseudo}</Text>
          <View style={styles.itemsRow2}>
            <ScrollView horizontal>
              {hisObjects?.length >0 && hisObjects?.map((object: any) => (
                <TouchableOpacity key={object._id} onPress={() => addToReceipedItems(object)} style={[styles.imageContainer]}>
                <Image source={{ uri: object.images[0] }} style={styles.image} />
                <Text>{object.title}</Text>
              </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        </View>
        {/* //Add a button to validate the trade */}
        {trade?.status === 'accepted' && <View>
          <Text style={{color: 'green', fontWeight: 'bold', fontSize: 18, textAlign: 'center'}}>Proposition acceptée</Text>
        </View>}
        {trade?.status === 'rejected' && <View>
          <Text style={{color: 'red', fontWeight: 'bold', fontSize: 18, textAlign: 'center'}}>Proposition refusée</Text>
        </View>}
        {trade?.status === 'pending' && <View>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
            <TouchableOpacity onPress={acceptProposal} style={{
              backgroundColor: 'green',
              padding: 10,
              width: '40%',
              alignItems: 'center',
              borderRadius: 10,
            }}>
              <Text style={{fontWeight: 'bold', color: '#fff'}}>Accepter</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={refuseProposal} style={{
              backgroundColor: '#FF0000',
              padding: 10,
              width: '40%',
              alignItems: 'center',
              borderRadius: 10,
            }}>
              <Text style={{fontWeight: 'bold', color: '#fff'}}>Refuser</Text>
            </TouchableOpacity>
          </View>
        </View>
        }
           {!trade && <TouchableOpacity style={styles.sendTradeButton} onPress={validationHandler}>
            <Text style={{fontWeight: 'bold', color: '#fff'}}>Envoyer la proposition</Text>
          </TouchableOpacity>}

      </ScrollView>
    </SafeAreaView>
    </Background>
  );
};

// Define styles for the component
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
  },
  section: {
    marginBottom: 20,
    marginTop: 5,
  },
  sectionTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 10,
  },
  itemsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(90, 162, 245, 0.5)',
    padding: 20,
    borderRadius: 10,
    minHeight: 90,
  },
  itemsRow2: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(5, 74, 153, 0.5)',
    padding: 20,
    borderRadius: 10,
    minHeight: 90,
  },
  imageContainer: {
    width: 50,
    height: 50,
    marginRight: 10,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 15,
  },
  spacer: {
    flex: 1,
  },
  proposalContainer: {
    backgroundColor: 'rgba(90, 162, 245, 0.5)',
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    height: 150,
    zIndex: -1,
  },
  proposalContainerDark: {
    backgroundColor: 'rgba(5, 74, 153, 0.5)',
    padding: 20,
    marginBottom: 10,
    borderRadius: 10,
    height: 150,
    zIndex: -1,
  },
  proposalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
  },
  sendTradeButton: {
    backgroundColor: '#4B4CED',
    padding: 20,
    width: '80%',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});

export default TradeScreen;
