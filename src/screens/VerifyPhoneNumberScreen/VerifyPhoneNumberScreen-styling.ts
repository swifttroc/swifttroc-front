import styled from 'styled-components/native';
import { colors } from '../../colors';

const { primary, dark, light, gray, accent } = colors;

export const Container = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 20px;
    background-color: ${light};
`;

export const TitleText = styled.Text`
    font-size: 28px;
    color: ${dark};
    margin-bottom: 20px;
    font-weight: bold;
    text-align: center;
`;

export const InputField = styled.TextInput`
    border-width: 2px;
    border-radius: 12px;
    border-color: ${gray};
    width: 100%;
    height: 55px;
    background-color: ${light};
    color: ${dark};
    font-size: 20px;
    padding-left: 15px;
    margin-bottom: 20px;
    text-align: center;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
`;

export const CustomButton = styled.TouchableOpacity`
    background-color: ${primary};
    border-radius: 12px;
    padding: 15px;
    width: 100%;
    align-self: center;
    margin-top: 20px;
    shadow-color: ${dark};
    shadow-opacity: 0.25;
    shadow-radius: 8px;
    elevation: 5;
`;

export const ButtonText = styled.Text`
    color: ${light};
    text-align: center;
    font-weight: bold;
    font-size: 18px;
`;

export const InfoText = styled.Text`
    color: ${gray};
    font-size: 16px;
    text-align: center;
    margin-top: 10px;
`;

export const ErrorText = styled.Text`
    color: ${accent};
    font-size: 14px;
    text-align: center;
    margin-bottom: 20px;
`;
