import React, { useState, useRef } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import Background from '../../components/layers/Background';
import { useNavigation } from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import { UsersAPI } from '../../API/UsersAPI';

const VerifyPhoneNumberScreen = () => {
    const navigation = useNavigation();

    const [code, setCode] = useState<string[]>(['', '', '', '']);
    const [error, setError] = useState<string>('');
    const [success, setSuccess] = useState<string>('');

    const inputRefs = [
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
        useRef<TextInput>(null),
    ];

    const handleChangeText = (text: string, index: number) => {
        if (text.length > 1) return;

        const newCode = [...code];
        newCode[index] = text;
        setCode(newCode);

        if (text && index < 3) {
            inputRefs[index + 1].current?.focus();
        }

        if (index === 3 && text) {
            handleVerify(newCode.join(''));
        }
    };

    const handleVerify = async (verificationCode: string) => {
        UsersAPI.verifyPhone(verificationCode)
            .then((response: any) => {
                if (response.status === 200) {
                    setSuccess('Phone number verified successfully!');
                    setError('');
                    navigation.navigate('AuthScreen' as never);
                } else {
                    setError('Verification failed');
                    setSuccess('');
                }
            })
            .catch((error: any) => {
                console.log('[VERIFICATION_ERROR]', error);
                setError('Verification failed');
                setSuccess('');
                Toast.show({
                    type: 'error',
                    text1: 'Error',
                    text2: 'Verification failed',
                });
            });
    };

    return (
        <Background>
            <View style={styles.container}>
                <Text style={styles.title}>Verify Your Phone Number</Text>

                <View style={styles.codeContainer}>
                    {code.map((digit, index) => (
                        <TextInput
                            key={index}
                            ref={inputRefs[index]}
                            style={styles.input}
                            keyboardType="numeric"
                            maxLength={1}
                            value={digit}
                            onChangeText={(text) => handleChangeText(text, index)}
                        />
                    ))}
                </View>

                {error ? <Text style={styles.error}>{error}</Text> : null}
                {success ? <Text style={styles.success}>{success}</Text> : null}

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => handleVerify(code.join(''))}
                >
                    <Text style={styles.buttonText}>Verify</Text>
                </TouchableOpacity>
            </View>
        </Background>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 16,
    },
    title: {
        fontSize: 24,
        marginBottom: 16,
        textAlign: 'center',
    },
    codeContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        gap: 10,
        marginBottom: 20,
    },
    input: {
        width: 50,
        height: 50,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 10,
        textAlign: 'center',
        fontSize: 18,
    },
    button: {
        backgroundColor: '#007bff',
        padding: 16,
        borderRadius: 4,
        alignItems: 'center',
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
    },
    error: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
    },
    success: {
        color: 'green',
        textAlign: 'center',
        marginBottom: 16,
    },
});

export default VerifyPhoneNumberScreen;
