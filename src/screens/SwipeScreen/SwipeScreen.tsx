import { View } from 'react-native'
import React, { useCallback, useState } from 'react'
import { useFocusEffect } from '@react-navigation/native';

import Background from '../../components/layers/Background'
import { ObjectsAPI } from '../../API/ObjectsAPI';
import { Swipe } from '../../components/shared/Swipe';
import { useActiveScreen } from '../../context/ScreenContext';


const SwipeScreen = () => {
  const [objects, setObjects] = useState([]);
  const { currentUser } = useActiveScreen();

  useFocusEffect(
    useCallback(() => {
      getObjects();
    }, [])
  );
  
  const getObjects = async () => {
    try {
      const objects = await ObjectsAPI.GET_FILTERED({   
        localisation: currentUser ? { latitude: currentUser.localisation.latitude, longitude: currentUser.localisation.longitude } : undefined,
        radius: 60
      });
      setObjects(objects);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <Background>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
         {objects.length > 0 && <Swipe products={objects} />}
      </View>
    </Background>
  )
}

export default SwipeScreen