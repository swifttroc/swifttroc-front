import { View, Text, ScrollView } from 'react-native'
import React from 'react'
import Background from '../../components/layers/Background'
import WishList from '../../components/shared/WishList'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useActiveScreen } from '../../context/ScreenContext'

const WishListScreen = () => {

  const { wishlist, setWishlist } = useActiveScreen();

  return (
    <Background>
        <ScrollView style={{paddingTop: 40, marginTop: 60, marginBottom: 20}}>
            <Text style={{
                    marginLeft: 10,
                    fontSize: 20,
                    fontWeight: 'bold',
                }}>
                Mes objets Favoris
            </Text>
            <WishList />
            
            {wishlist && wishlist?.length > 0 && 
            <TouchableOpacity onPress={() => setWishlist([])} style={{
                alignSelf: 'center',
                padding: 10,
                paddingHorizontal: 20,
                backgroundColor: '#fa6132',
                borderRadius: 5,
                marginTop: 20,
            }}>
                <Text style={{
                    color: 'white',
                    textAlign: 'center',
                }}>Tout supprimer</Text>
            </TouchableOpacity>}

            <View style={{height: 40}}></View>
            
        </ScrollView>
    </Background>
  )
}

export default WishListScreen