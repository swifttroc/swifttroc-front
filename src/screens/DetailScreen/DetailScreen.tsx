import React, { useState, useContext, useEffect } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity, StyleSheet, Modal, FlatList } from 'react-native';
import { RouteProp, useRoute, useNavigation, NavigationProp } from '@react-navigation/native';
import Background from '../../components/layers/Background';
import { RootStackParamList } from '../../components/layers/AppNavigator';
import { useActiveScreen } from '../../context/ScreenContext';


interface Object {
  _id: string;
  title: string;
  description: string;
  categories: string[];
  images: string[];
  publicationDate: Date;
  owner: string;
  isTraded: boolean;
}

type DetailScreenRouteProp = RouteProp<{ DetailScreen: { object?: any } }, 'DetailScreen'>;

const DetailScreen = () => {
  const route = useRoute<DetailScreenRouteProp>();
  const { object } = route.params || {};
  const { currentUser, wishlist, removeFromWishlist, addToWishlist } = useActiveScreen();


  const [isModalVisible, setModalVisible] = useState(false);
  const [selectedImageIndex, setSelectedImageIndex] = useState(0);
  const navigation = useNavigation<NavigationProp<RootStackParamList>>();

  const openImageModal = (index: number) => {
    setSelectedImageIndex(index);
    setModalVisible(true);
  };

    const inWishList = (id: string) => {
      const index = wishlist.findIndex((item: any) => item._id === id);
      return index===-1 ? false : true;
    };

  useEffect(() => {
    if (currentUser) {
      console.log('Current User ID:', currentUser._id);
    } else {
      console.log('No user is currently logged in.');
    }
  }, [currentUser]);

  return (
    <Background>
      <ScrollView contentContainerStyle={styles.container} style={{ paddingHorizontal: 10, paddingTop: 10, marginTop: 60, marginBottom: 18 }}>
        <View style={{
          width: '100%',
          alignSelf: 'center',
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 20,
        }}>
          <TouchableOpacity onPress={() => openImageModal(0)}>
            <Image
              source={{ uri: object.images[0] }}
              style={{
                width: object.images.length > 3 ? 150 : object.images.length > 1 ? 200 : 300,
                height: object.images.length > 3 ? 150 : object.images.length > 1 ? 200 : 300,
                borderRadius: 10,
              }}
            />
          </TouchableOpacity>

          <View style={{
            flexDirection: object.images.length > 3 ? 'row' : 'column',
            flexWrap: 'wrap',
            width: '90%',
            maxWidth: '50%',
          }}>
            {object.images.slice(1).map((image: any, index: number) => (
              <TouchableOpacity key={index} onPress={() => openImageModal(index + 1)}>
                <Image source={{ uri: image }} style={{
                  width: object.images.length > 3 ? 75 : object.images.length > 1 ? 100 : 150,
                  height: object.images.length > 3 ? 75 : object.images.length > 1 ? 100 : 150,
                  borderRadius: 10,
                  marginLeft: 5,
                }} />
              </TouchableOpacity>
            ))}
          </View>
        </View>

        <View style={{
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
          <Text style={styles.title}>{object.title}</Text>
          {inWishList(object._id) ?
            <TouchableOpacity onPress={() => removeFromWishlist(object._id)}>
              <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}>❤️</Text>
            </TouchableOpacity>
            :
            <TouchableOpacity onPress={() => addToWishlist(object)}>
              <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}>🤍</Text>
            </TouchableOpacity>
          }
        </View>
        <View style={styles.categoryBadge}>
          <Text style={styles.categoryText}>{object.categories[0]}</Text>
        </View>

        <Text numberOfLines={6} style={styles.description}>{object.description}</Text>
        {object.owner !== currentUser?._id && (
          <TouchableOpacity style={styles.actionButton} onPress={() => navigation.navigate('TradeScreen', { Object: object, OwnerId: object.owner } as never)}>
            <Text style={styles.actionButtonText}>Demande d'échange</Text>
          </TouchableOpacity>
        )}


        {/* Modal pour afficher les images en grand */}
        <Modal visible={isModalVisible} transparent={true}>
          <View style={styles.modalContainer}>
            <View style={styles.flatListWrapper}>
              <FlatList
                data={object.images}
                horizontal
                pagingEnabled
                initialScrollIndex={selectedImageIndex}
                snapToAlignment="center"
                snapToInterval={300} // Assuming the width of each image
                decelerationRate="fast"
                contentContainerStyle={{ paddingHorizontal: 20 }} // Adding padding to center the last item
                getItemLayout={(data, index) => (
                  { length: 300, offset: 300 * index, index }
                )}
                renderItem={({ item }) => (
                  <Image
                    source={{ uri: item }}
                    style={styles.fullscreenImage}
                  />
                )}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            <TouchableOpacity onPress={() => setModalVisible(false)} style={styles.closeButton}>
              <Text style={styles.closeButtonText}>Fermer</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    alignItems: 'flex-start',
    height: '100%',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'left',
    marginBottom: 10,
    color: '#fff',
  },
  categoryBadge: {
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: '#00A896',
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignSelf: 'flex-start',
    marginBottom: 20,
  },
  categoryText: {
    color: '#fff',
    fontSize: 14,
  },
  description: {
    fontSize: 16,
    color: '#fff',
    fontStyle: 'italic',
    textAlign: 'center',
    marginBottom: 30,
  },
  actionButton: {
    borderWidth: 1,
    borderColor: '#fff',
    position: 'absolute',
    backgroundColor: '#007BFF',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 15,
    alignSelf: 'center',
    bottom: 40,
  },
  actionButtonText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  flatListWrapper: {
    justifyContent: 'center', // Centrer verticalement
    alignItems: 'center',
    flex: 1,
    marginTop: 150, // Ajout d'une marge supérieure
  },
  fullscreenImage: {
    width: 300,
    height: 300,
    marginHorizontal: 10,
  },
  closeButton: {
    marginTop: 10, // Réduit pour rapprocher du FlatList
    backgroundColor: '#007BFF',
    padding: 15,
    borderRadius: 10,
    alignSelf: 'center',
  },
  closeButtonText: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default DetailScreen;
