import { View, Text, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import React from 'react'
import Background from '../../components/layers/Background'
import { useNavigation } from '@react-navigation/native';
import { useActiveScreen } from '../../context/ScreenContext';
import CheckBox from 'react-native-check-box';

const AddObjectScreen: React.FC<any>  =  () => {
  const navigation = useNavigation();
  const { objectToAdd, setObjectToAdd } = useActiveScreen();
  const categoriesList = ['Livres', 'Vêtements', 'Meubles', 'Electronique', 'Jouets', 'Autres'];

  const handleContinue = () => {
    console.log('Object to add:', objectToAdd);
    navigation.navigate('AddObjectScreenTwo' as never);
  };

  const handleChange = (name: string, value: string) => {
    setObjectToAdd((prevState: any) => ({
        ...prevState,
        [name]: value
    }))
  }

  const handleCheck = (category: string) => {
    const categories = objectToAdd.categories;
    if (categories.includes(category)) {
      const index = categories.indexOf(category);
      categories.splice(index, 1);
    } else {
      categories.push(category);
    }

    setObjectToAdd((prevState: any) => ({
      ...prevState,
      categories: categories
    }))
  }

  return (
    <Background>
      <ScrollView style={{flex: 1, maxHeight: '94%'}} contentContainerStyle={{flexGrow:1, justifyContent: "center", padding: 20, marginTop: 20}}>
        
        <View style={{
          padding: 10,
          borderColor: '#b1afaf',
          borderWidth: 1,
          borderRadius: 10,
        }}>
          <Text style={{
            textAlign: 'center',
            fontSize: 24,
            fontWeight: 'bold',
            color: '#fff',
          }}>
            Quel est le titre de votre Objet ?
          </Text>
          <TextInput
            style={{
              textAlign: 'center',
              borderRadius: 50,
              borderColor: '#fff',
              borderWidth: 1,
              backgroundColor: '#17608f',
              padding: 6,
              marginVertical: 14,
            }}
            value={objectToAdd.title}
            onChangeText={(text) => handleChange('title', text)}
            placeholder="Titre de l'objet"
          />
        </View>
        <View style={{
          marginTop: 10,
          padding: 10,
          borderColor: '#b1afaf',
          borderWidth: 1,
          borderRadius: 10,
          maxHeight: "70%",
        }}>
          <ScrollView style={{maxHeight: 280}} nestedScrollEnabled={true}>
          <Text style={{
            textAlign: 'center',
            fontSize: 22,
            fontWeight: 'bold',
            color: '#fff',
            marginBottom: 10,
            }}>
              Selectionnez une ou plusieurs catégories
          </Text>
          {categoriesList.map((category, index) => (
            <View key={index} style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 2,
            }}>

              <CheckBox
                style={{flex: 1}}
                onClick={() => handleCheck(category)}
                checkBoxColor='#fff'
                checkedCheckBoxColor='#699f4c'
                isChecked={objectToAdd.categories.includes(category)}
                rightTextView={
                  <View style={{
                    marginLeft: 20,
                    backgroundColor: objectToAdd.categories.includes(category) ? '#1e6bb1' : 'transparent',
                    paddingVertical: 10,
                    paddingHorizontal: 20, 
                    borderRadius: 50,
                    width: "70%",
                  }}>
                    <Text style={{color: '#fff', fontSize: 16}}>
                    {category}
                    </Text>
                  </View>}
              />
            </View>
          ))}
          </ScrollView>
        </View>

        <TouchableOpacity
        disabled={objectToAdd.title === '' || objectToAdd.categories.length === 0}
        style={{
          opacity: objectToAdd.title === '' || objectToAdd.categories.length === 0 ? 0.5 : 1,
          backgroundColor: '#1e6bb1',
          borderColor: '#fff',
          borderWidth: 1,
          padding: 8,
          borderRadius: 50,
          marginTop: 20,
          alignItems: 'center',
          width: 150,
          alignSelf: 'flex-end',
        }} onPress={handleContinue as never}>
          <Text>Suivant</Text>
        </TouchableOpacity>

        <Text style={{
          marginTop: 16,
          paddingHorizontal: 20,
          fontSize: 16,
          color: '#fff',
          fontStyle: 'italic',
        }}>
          Un titre précis et une catégorie associée permettent aux futurs acheteurs de mieux cibler votre annonce !
        </Text>
      </ScrollView>
    </Background>
  )
}

export default AddObjectScreen