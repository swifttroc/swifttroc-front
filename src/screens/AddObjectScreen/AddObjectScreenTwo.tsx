import { View, Text, TextInput, ScrollView, TouchableOpacity, Image, Dimensions, TouchableWithoutFeedback, PermissionsAndroid  } from 'react-native'
import React, { useCallback, useState } from 'react'
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { useActiveScreen } from '../../context/ScreenContext';
import Background from '../../components/layers/Background';
import PlusCircleIcon from '../../components/icons/PlusCircleIcon';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { Alert } from 'react-native';
import { ObjectsAPI } from '../../API/ObjectsAPI';
import Toast from 'react-native-toast-message';
import TrashIcon from '../../components/icons/TrashIcon';

const AddObjectScreenTwo = () => {

  const navigation = useNavigation();
  const { objectToAdd, setObjectToAdd } = useActiveScreen();
  const [images, setImages] = useState<any[]>([]);

  useFocusEffect(
    useCallback(() => {
      if (objectToAdd.images.length > 0) {
        setImages(objectToAdd.images);
      }
    }, [])
  );

  const handleChange = (name: string, value: string) => {
    setObjectToAdd((prevState: any) => ({
        ...prevState,
        [name]: value
    }))
  }

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Camera Permission',
          message: 'Cette application a besoin de votre permission pour accéder à la caméra',
          buttonNeutral: 'Demande plus tard',
          buttonNegative: 'Annuler',
          buttonPositive: 'Accepter',
        }
      );
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert("Accès refusé", "Vous devez autoriser l'accès à la caméra pour prendre des photos");
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const handleAddImage = () => {
    requestCameraPermission();
    Alert.alert(
      "Séléctionner des images",
      "Choissisez des images à partir de la galerie ou prenez des photos",
      [
        {
          text: "Annuler",
          style: "cancel"
        },
        {
          text: "Prendre une photo",
          onPress: () => launchCamera(
            { mediaType: 'photo', cameraType: 'back', saveToPhotos: true }, // `selectionLimit: 0` allows multiple selection
            (response) => {
              if (response.assets && response.assets.length > 0) {
                setImages([...images, response.assets[0].uri]);
              }
            }
          )
        },
        {
          text: "Choisir dans la galerie",
          onPress: () => launchImageLibrary(
            { mediaType: 'photo', selectionLimit: 0 }, // `selectionLimit: 0` allows multiple selection
            (response) => {
              if (response.assets) {
                const newImages = response.assets.map(asset => asset.uri);
                setImages([...images, ...newImages]);
              }
            }
          )
        }
      ]
    );
  };

  const saveObject = () => {
    if (images.length === 0) {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Erreur',
        text2: 'Veuillez ajouter au moins une image',
        visibilityTime: 3000,
        autoHide: true,
      });
      return;
    }
    setObjectToAdd((prevState: any) => ({
      ...prevState,
      images: images
    }))

    const formData = new FormData();
    formData.append('title', objectToAdd.title);
    formData.append('description', objectToAdd.description);
    formData.append('categories', objectToAdd.categories.join(','));
    images.forEach((uri, index) => {
      const imageName = uri.split('/').pop();
      formData.append('images', {
          uri: uri,
          name: imageName || `image_${index}.jpg`,
          type: 'image/jpeg',
      });
  });

    console.log('Object Save:', objectToAdd);
   
    ObjectsAPI.POST(formData)
    .then((response: any) => {
      //go to catch block if status is not 200
      if(!response?.status || response?.status != 200) throw new Error(response?.data?.message);
      Toast.show({
        type: 'success',
        position: 'top',
        text1: 'Objet ajouté avec succès',
        visibilityTime: 3000,
        autoHide: true,
      });
      setObjectToAdd({title: '', description: '', categories: [], images: []});
      setImages([]);
      navigation.navigate('HomeScreen' as never);
    })
    .catch((error: any) => {
      Toast.show({
        type: 'error',
        position: 'top',
        text1: 'Erreur lors de l\'ajout de l\'objet',
        text2: error.message,
        visibilityTime: 4000,
        autoHide: true,
      });
    })
  }
  

  return (
    <Background>
      <ScrollView style={{flex: 1, maxHeight: '94%'}} contentContainerStyle={{flexGrow:1, justifyContent: "center", padding: 20, marginTop: 20}}>
        
        <View style={{
          borderColor: '#b1afaf',
          borderWidth: 1,
          borderRadius: 10,
          position: 'relative',
          height: 300,
        }}>
          <Text style={{
            marginTop: 20,
            textAlign: 'center',
            fontSize: 24,
            fontWeight: 'bold',
            color: '#fff',
          }}>
           Sélectionner des images
          </Text>
          
          {images.length > 0 ? (
          <TouchableWithoutFeedback>
            <ScrollView
              horizontal
              pagingEnabled
              showsHorizontalScrollIndicator={false}
              style={{
                flex: 1,
                borderRadius: 10,
                alignSelf: 'center',
                position: 'absolute',
                width: '100%',
                height: '100%',
                zIndex: 1,
                top: 0,
                left: 0,
              }}
            >
              {images.map((uri, index) => (
                <Image
                  key={index}
                  source={{ uri }}
                  style={{
                    width: Dimensions.get('window').width - 42, // Adjust to fit within the container padding
                    // width: '100%',
                    height: '100%',
                    borderRadius: 10,
                  }}
                />
              ))}
            </ScrollView>
          </TouchableWithoutFeedback>
        ) : (
          <TouchableOpacity
            onPress={handleAddImage}
            style={{
              alignSelf: 'center',
              position: 'absolute',
              width: '100%',
              height: '100%',
              zIndex: 1,
              top: 0,
              left: 0,
            }}>
            <View style={{
              position: 'absolute',
              top: '50%',
              left: '50%',
              transform: [{ translateX: -30 }, { translateY: -30 }],
            }}>
              <PlusCircleIcon size={60} color='#fff' />
            </View>
          </TouchableOpacity>
        )}
        </View>
        {images.length > 0 && 
          <TouchableOpacity style={{alignSelf: 'center', marginVertical: 8}} onPress={() => setImages([])}>
            <TrashIcon size={40} />
          </TouchableOpacity>
        }

        <View style={{
          padding: 10,
          borderColor: '#b1afaf',
          borderWidth: 1,
          borderRadius: 10,
          marginTop: 10,
        }}>
          <Text style={{
            textAlign: 'center',
            fontSize: 24,
            fontWeight: 'bold',
            color: '#fff',
          }}>
           {objectToAdd.title}
          </Text>
          <Text style={{
            textAlign: 'center',
            fontSize: 18,
            fontWeight: 'semibold',
            color: '#fff',
            fontStyle: 'italic',
          }}>
           {objectToAdd.categories.join(', ')}
          </Text> 
         
          <TextInput
            style={{
              textAlign: 'left',
              marginTop: 20,
              textAlignVertical: 'top',
              borderRadius: 10,
              borderColor: '#fff',
              borderWidth: 1,
              backgroundColor: '#17608f',
              padding: 6,
              minHeight: 140,
              color: '#fff',
            }}
            value={objectToAdd.description}
            onChangeText={(text) => handleChange('description', text)}
            placeholder="Description"
            placeholderTextColor="#ccc"
            multiline={true}
          />
          </View>

          <TouchableOpacity style={{
            backgroundColor: '#1e6bb1',
            padding: 10,
            borderColor: '#fff',
            borderWidth: 1,
            borderRadius: 20,
            marginTop: 20,
            width: '50%',
            alignSelf: 'center',
          }} onPress={saveObject}>
            <Text style={{
              textAlign: 'center',
              fontSize: 22,
              fontWeight: 'bold',
              color: '#fff',
            }}>
              Sauvegarder
            </Text>
          </TouchableOpacity>
      </ScrollView>
    </Background>
  )
}



export default AddObjectScreenTwo