import { View, TextInput, TouchableOpacity, Text, Modal, Pressable, ScrollView } from 'react-native';
import React, { useState, useCallback, useEffect } from 'react';
import Background from '../../components/layers/Background';
import { ObjectsAPI } from '../../API/ObjectsAPI';
import { useFocusEffect } from '@react-navigation/native';
import ObjectCard from '../../components/shared/ObjectCard';
import { useNavigation, NavigationProp } from '@react-navigation/native';
import { RootStackParamList } from '../../components/layers/AppNavigator';
import SearchIcon from '../../components/icons/SearchIcon';
import FilterIcon from '../../components/icons/FilterIcon';
import Slider from '@react-native-community/slider'; 
import { useActiveScreen } from '../../context/ScreenContext';
import Toast from 'react-native-toast-message';

interface Object {
  _id: string;
  title: string;
  description: string;
  categories: string[];
  images: string[];
  publicationDate: Date;
  owner: string;
  isTraded: boolean;
}

const categoriesList = ['Livres', 'Vêtements', 'Meubles', 'Electronique', 'Jouets', 'Autres'];

function useDebounce(value: string, delay: number) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}

const SearchScreen = () => {
  const [objects, setObjects] = useState<Object[]>([]);
  const [category, setCategory] = useState<string[]>([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [radius, setRadius] = useState(30); // Rayon de recherche en kilomètres
  const { currentUser } = useActiveScreen();
  const debouncedSearchQuery = useDebounce(searchQuery, 300);
  const navigation = useNavigation<NavigationProp<RootStackParamList>>();


  useFocusEffect(
    useCallback(() => {
      //wait 1s when radius change to avoid multiple requests
      const timer = setTimeout(() => {
        getFilteredObjects();
      }, 1000);
      return () => clearTimeout(timer);

    }, [category, debouncedSearchQuery, radius, currentUser?.location?.latitude, currentUser?.location?.longitude]) // Ajouter radius comme dépendance
  );

  const getFilteredObjects = async () => {
    setIsLoading(true);
    try {
      const latitude = currentUser?.location.latitude ? currentUser.location.latitude.toString() : null;
      const longitude = currentUser?.location.longitude ? currentUser.location.longitude.toString() : null;

 
      // Utiliser les coordonnées récupérées de currentUser pour userLocation
    const userLocation = {
        latitude: latitude ? parseFloat(latitude) : 48.9115,   // Si currentUser est défini, utiliser ses coordonnées
        longitude: longitude ? parseFloat(longitude) : 2.1969,  // Sinon, utiliser des coordonnées en dur
    };

      const objects = await ObjectsAPI.GET_FILTERED({ 
        title: debouncedSearchQuery, 
        description: debouncedSearchQuery, 
        category: category.join(','),
        localisation: userLocation ? { latitude: userLocation.latitude, longitude: userLocation.longitude } : undefined,
        radius: radius // Ajoutez le rayon en kilomètres
      });
      setObjects(objects);
    } catch (error) {
      console.log(error);
      Toast.show({
        type: 'error',
        text1: 'Erreur',
        text2: 'Veuillez vérifier que votre localisation est activée',
      });
    } finally {
      setIsLoading(false);
    }
  };

  const toggleCategory = (selectedCategory: string) => {
    if (category.includes(selectedCategory)) {
      setCategory(category.filter(c => c !== selectedCategory));
    } else {
      setCategory([...category, selectedCategory]);
    }
  };

  const removeCategory = (removedCategory: string) => {
    setCategory(category.filter(c => c !== removedCategory));
  };

  return (
    <Background>
      <ScrollView style={{ paddingHorizontal: 10, paddingTop: 10, marginTop: 60, marginBottom: 18 }}>
        {/* Barre de recherche */}
        <View style={{ flexDirection: 'row', marginBottom: 20, alignItems: 'center' }}>
          <View style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            borderColor: '#ccc',
            borderWidth: 1,
            borderRadius: 25,
            backgroundColor: '#007BFF',
            paddingHorizontal: 10,
          }}>
            <SearchIcon size={24} color='#bcb6b6' />
            <TextInput
              value={searchQuery}
              onChangeText={(text) => setSearchQuery(text)}
              placeholder="Recherche"
              placeholderTextColor="#fff"
              style={{
                flex: 1,
                paddingVertical: 10,
                paddingHorizontal: 10,
                color: '#fff',
                marginLeft: 10,
              }}
            />
            <TouchableOpacity onPress={() => setModalVisible(true)}>
              <FilterIcon size={24} color='#000' />
            </TouchableOpacity>
          </View>
        </View>

        {/* Selected categories display as bubbles */}
        <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginBottom: 20 }}>
          {category.map((cat, index) => (
            <View key={index} style={{
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: '#007BFF',
              borderRadius: 20,
              paddingHorizontal: 10,
              paddingVertical: 5,
              margin: 5,
            }}>
              <Text style={{ color: '#fff', marginRight: 10 }}>{cat}</Text>
              <TouchableOpacity onPress={() => removeCategory(cat)}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>✕</Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>

        {/* Modal de sélection des filtres */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => setModalVisible(false)}
        >
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
            <View style={{ width: 300, backgroundColor: 'white', borderRadius: 10, padding: 20 }}>
              <Text style={{ fontWeight: 'bold', marginBottom: 10 }}>Sélectionnez les catégories</Text>
              <ScrollView>
                {categoriesList.map((categoryItem, index) => (
                  <TouchableOpacity key={index} onPress={() => toggleCategory(categoryItem)}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
                      <Text style={{color: "#000"}}>{categoryItem}</Text>
                      <Text style={{ marginLeft: 'auto' }}>{category.includes(categoryItem) ? '✅' : '⬜'}</Text>
                    </View>
                  </TouchableOpacity>
                ))}
              </ScrollView>

              {/* Localisation et Rayon de recherche */}
              <Text style={{ fontWeight: 'bold', marginTop: 20, color: "#000" }}>Localisation (rayon de recherche)</Text>
              <Text style={{ marginVertical: 10, color: "#000" }}>Rayon : {radius} km</Text>
              <Slider
                minimumValue={1}
                maximumValue={80}
                step={1}
                value={radius}
                onValueChange={(value) => setRadius(value)}
                minimumTrackTintColor="#007BFF"
                maximumTrackTintColor="#ccc"
                thumbTintColor="#007BFF"
              />

              <Pressable
                style={{ marginTop: 10, padding: 10, backgroundColor: '#007BFF', borderRadius: 5 }}
                onPress={() => setModalVisible(false)}
              >
                <Text style={{ color: '#fff', textAlign: 'center' }}>Valider</Text>
              </Pressable>
            </View>
          </View>
        </Modal>

        {/* Affichage des résultats */}
        {isLoading ? (
          <Text>Chargement...</Text>
        ) : (
          <View style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'space-around', // Réduit l'espacement entre les éléments
            marginTop: 20,
          }}>
            {objects.length > 0 ? objects.map((object: Object) => (
              <TouchableOpacity 
                onPress={() => navigation.navigate('DetailScreen', { object })}
                key={object._id}
                style={{
                  width: '45%',  // Garder la largeur à 45%
                  marginBottom: 10, // Ajout de marge en bas
                  height: 260,
                }}
              >
                <ObjectCard 
                  object={object}
                  size='small'
                />
              </TouchableOpacity>
            )) : (
              <Text style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
              }}>
                {(!currentUser?.location?.latitude && !currentUser?.location?.longitude)? "Activez la localisation pour voir les objets autour de vous" : "Il n'y a pas d'objets dans votre rayon de recherche"}
              </Text>
            )}
          </View>
        )}
      </ScrollView>
    </Background>
  );
};

export default SearchScreen;
