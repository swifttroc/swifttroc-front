import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, ScrollView, TouchableOpacity, Text } from 'react-native';

import Background from '../../components/layers/Background';
import AppTitle from '../../components/shared/AppTitle';
import * as Styling from './RegisterScreen-styling';
import { UsersAPI } from '../../API/UsersAPI';
import Toast from 'react-native-toast-message';
import EmailIcon from '../../components/icons/EmailIcon';
import PhoneIcon from '../../components/icons/PhoneIcon';
import { LockPasswordIcon, UnlockPasswordIcon } from '../../components/icons/PasswordIcons';


const RegisterScreen = () => {
    const [name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [pseudo, setPseudo] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [emailError, setEmailError] = useState('');
    const navigation = useNavigation();
    const [showPassword, setShowPassword] = useState(false);

      // Validation simple de l'email
      const validateEmail = (email: string) => {
        if (!email.includes('@')) {
            setEmailError('Veuillez entrer une adresse email valide.');
            return false;
        } else {
            setEmailError('');
            return true;
        }
    };

  // Fonction appelée lors de l'inscription
  const handleRegister = async () => {
    console.log('Registering...');
    // Vérifier les champs
    if (!name || !lastName || !pseudo || !email || !phone || !password) {
        Toast.show({
            type: 'error',
            text1: 'Erreur',
            text2: 'Veuillez remplir tous les champs.',
        });
        return;
    }

    // Valider l'email
    if (!validateEmail(email)) {
        return;
    }

    // Envoyer les données au backend
    const userData = { name, lastName, pseudo, email, phone, password };

    UsersAPI.startRegistration(userData)
    .then((response: any) => {
        if (response.status === 200) {
            console.log('Registration success');
            navigation.navigate('VerifyPhoneNumberScreen' as never);
        }
        else {throw new Error(response.data.message || "Erreur lors de l'inscription");}
    })
    .catch((error: any) => {
        const errorMessage = error.response?.data?.message || error.message || 'Un problème est survenu';
        Toast.show({
            type: 'error',
            text1: 'Error',
            text2: 'Erreur: ' + errorMessage,
        });
    });
};


  return (
    <Background>
    <AppTitle/>
    <View style={{
      width: '100%',
      height: '80%',
    }}>
      <ScrollView style={{
          borderWidth: 1,
          borderColor: 'white',
          borderRadius: 20,
          width: '90%',
          height: '100%',
          alignSelf: 'center',
      }} contentContainerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
      }}>
        <Styling.TitleText>Inscription</Styling.TitleText>

        <Styling.InputField
          placeholder="Prénom"
          value={name}
          onChangeText={setName}
           placeholderTextColor="white"
        />
        <Styling.InputField
          placeholder="Nom"
          value={lastName}
          onChangeText={setLastName}
           placeholderTextColor="white"
        />
        <Styling.InputField
          placeholder="Pseudo"
          value={pseudo}
          onChangeText={setPseudo}
           placeholderTextColor="white"
        />
        <View style={{
            width: '100%',
            position: 'relative',
            alignItems: 'center',
        }}>
          <View style={{
            position: 'absolute',
            left: 30,
            top: 10,
          }}>
            <EmailIcon size={30} />
          </View>
          <Styling.InputField
            placeholder="Email"
            value={email}
            onChangeText={setEmail}
            keyboardType="email-address"
            placeholderTextColor="white"
          />
        </View>
        {emailError ? <Styling.ErrorText>{emailError}</Styling.ErrorText> : null}
        <View style={{
            width: '100%',
            position: 'relative',
            alignItems: 'center',
        }}>
          <View style={{
            position: 'absolute',
            left: 30,
            top: 10,
          }}>
            <PhoneIcon size={30} />
          </View>
          <Styling.InputField
            placeholder="Téléphone"
            value={phone}
            onChangeText={setPhone}
            keyboardType="phone-pad"
            placeholderTextColor="white"
          />
        </View>
        <View style={{
            width: '100%',
            position: 'relative',
            alignItems: 'center',
        }}>
          <TouchableOpacity onPress={() => setShowPassword(!showPassword)} style={{
            position: 'absolute',
            left: 30,
            top: 10,
            zIndex: 1
          }}>
            {showPassword ? <LockPasswordIcon size={30} color='#fff' /> : <UnlockPasswordIcon size={30} color='#fff' />}
          </TouchableOpacity>
          <Styling.InputField
            placeholder="Mot de passe"
            value={password}
            onChangeText={setPassword}
            secureTextEntry={!showPassword}
            placeholderTextColor="white"
          />
        </View>

      <Styling.CustomButton onPress={handleRegister}>
          <Styling.ButtonText>Enregister</Styling.ButtonText>
      </Styling.CustomButton>

      <View style={{
          flexDirection: 'row',
          marginTop: 20,
          alignItems: 'center',
      }}>
        <Text>Déjà inscrit ? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('AuthScreen' as never)}>
        <Text style={{fontWeight: 'bold', marginTop: 2}}>Se connecter</Text> 
        </TouchableOpacity>
      </View>

      </ScrollView>
      </View>
    </Background>
  );
};

export default RegisterScreen;