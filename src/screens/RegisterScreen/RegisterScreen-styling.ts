import styled from 'styled-components/native';
import { colors } from '../../colors';

const { primary, dark } = colors;


export const Container = styled.View`
  flex: 1;
  justify-content: space-around;
  align-items: center;
  background-color: transparent;
  border-width: 1.5px;
  border-color: white;
  border-radius: 20px;
  width: 90%;
  height: 70%;
  align-self: center;
  margin-top: 20px;
`;

export const TitleText = styled.Text`
  color: white;
  font-size: 30px;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  margin: 20px;
`;

export const InputContainer = styled.View`
  width: 100%;
  padding: 20px;
`;

export const InputField = styled.TextInput`
  border-width: 1.5px;
  border-color: white;
  border-radius: 10px;
  padding: 10px;
  margin-bottom: 15px;
  color: white;
  background-color: transparent;
  width: 90%;
  text-align: center;  /* Centre le texte et le placeholder */
  font-size: 16px;    /* Taille de police pour le texte */
`;

export const CustomButton = styled.TouchableOpacity`
  border-color: white;
  border-width: 1.5px;
  background-color: #62a7f0;
  border-radius: 18px;
  padding: 10px;
  width: 54%;
  align-self: center;
  margin: 20px;
`;

export const ButtonText = styled.Text`
  text-align: center;
  font-weight: bold;
  font-size: 16px;
  color: white;
`;

export const ErrorText = styled.Text`
  color: red;
  font-size: 12px;
  text-align: center;
  margin-bottom: 10px;
`;