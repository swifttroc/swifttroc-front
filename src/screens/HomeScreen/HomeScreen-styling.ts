import styled from 'styled-components/native';
import { colors } from '../../colors';

const { primary, dark, light } = colors;

export const CustomView = styled.View`
  padding: 10px;
  flex: 1;
  background-color: ${light};
`;

export const HeaderBackground = styled.View`
  /* background-color: ${primary}; */
  padding: 15px;
`;

export const Title = styled.Text`
  font-size: 28px;
  font-weight: bold;
  color: ${dark};
  text-align: center;
`;

export const SearchInput = styled.TextInput`
  color: #000;
  flex: 1;
  padding: 5px 10px;
  font-size: 16px;
`;

export const SearchBarWrapper = styled.View`
  color: #000;
  flex-direction: row;
  align-items: center;
  background-color: white;
  border-radius: 20px;
  padding: 5px;
  margin-top: 10px;
`;

export const FilterIcon = styled.Image`
  width: 20px;
  height: 20px;
  margin: 5px;
`;

export const ButtonsContainer = styled.View`
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 15px;
`;

export const FilterButton = styled.TouchableOpacity`
  background-color: white;
  padding: 8px 12px;
  border-radius: 20px;
  border: 1px solid ${primary};
`;

export const FilterButtonText = styled.Text`
  font-size: 14px;
  color: ${primary};
`;


export const ItemText = styled.Text`
  font-size: 16px;
  color: ${primary};
  margin-top: 10px;
`;

export const PriceText = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: ${dark};
  margin-top: 5px;
`;

export const FavoriteIcon = styled.Image`
  width: 20px;
  height: 20px;
  margin-top: 5px;
`;

export const NavBar = styled.View`
  flex-direction: row;
  justify-content: space-around;
  padding: 10px 0;
  background-color: ${primary};
  align-items: center;
`;

export const NavIcon = styled.Image`
  width: 24px;
  height: 24px;
`;

export const UploadIcon = styled.Image`
  width: 24px;
  height: 24px;
  background-color: ${light};
  border-radius: 50px;
  padding: 10px;
`;

export const ProfileIcon = styled.Image`
  width: 40px;
  height: 40px;
  border-radius: 20px;
`;

export const ItemsContainer = styled.View`
  width: 100%;
  justify-content: center;
  padding: 10px;
`;

export const ItemCard = styled.View`
  width: 48%;  /* To fit two cards per row with some spacing */
  margin-bottom: 15px;
  background-color: white;
  border-radius: 10px;
  padding: 10px;
  align-items: center;
  elevation: 3;
`;

