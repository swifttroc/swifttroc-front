import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import Background from '../../components/layers/Background';
import * as Styling from './HomeScreen-styling';
import { useActiveScreen } from '../../context/ScreenContext';
import { getPhoneLocation, getAddressFromLocation, getBoundingBox } from '../../utils/function';
import Toast from 'react-native-toast-message';
import SearchIcon from '../../components/icons/SearchIcon';
import WishList from '../../components/shared/WishList';
import AppTitle from '../../components/shared/AppTitle';
import MapView, { Marker } from 'react-native-maps';
import LocationIcon from '../../components/icons/LocationIcon';
import SlidingTextBanner from '../../components/shared/SlidingTextBanner';
import { UsersAPI } from '../../API/UsersAPI';

const LandingScreen = () => {
  const { currentUser, setCurrentUser, setActiveScreen } = useActiveScreen();
  const navigation = useNavigation();
  const [location, setLocation] = useState<{ latitude: number; longitude: number } | any>(null);
  const [address, setAddress] = useState<{
    road?: string;
    postalCode?: string;
    city?: string;
    country?: string;
  } | null>(null);
  
  const [initialRegion, setInitialRegion] = useState<any | null>({
    latitude: currentUser?.location?.latitude || 37.78825,
    longitude: currentUser?.location?.longitude || -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  useEffect(() => {
    fetchLocationAndAddress();
    fetchBoundingBox();
  }, []);

  useEffect(() => {
    if (currentUser?.location) {
      console.log('Current user change LOCATION:', currentUser.location);
      setInitialRegion({
        latitude: currentUser?.location?.latitude,
        longitude: currentUser?.location?.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      });
      updateUserLocation();
    }
  }, [currentUser?.location?.latitude, currentUser?.location?.longitude]);

  const fetchLocationAndAddress = async () => {
    try {
      const loc = await getPhoneLocation();
      setLocation(loc);

      if (loc) {
        const addr = await getAddressFromLocation(loc);
        console.log('Address:', addr);
        setAddress({
          road: addr.road,
          postalCode: addr.postalCode,
          city: addr.city,
          country: addr.country,
        });
        setCurrentUser({
          ...currentUser,
          location: {
            latitude: loc.latitude,
            longitude: loc.longitude,
            country: addr.country,
            city: addr.city,
            postalCode: addr.postalCode,
            road: addr.road,
          },
        });

        //update user location in database
        updateUserLocation();

      }
    } catch (err) {
      console.log('EREEEUR:', err);
      let error = err || 'Erreur lors de la récupération de la localisation';
      if(err === 'Position unavailable') error = 'Activez la localisation pour voir les annonces autour de vous';
      if(err === 'Permission denied') error = 'Acceptez la localisation pour voir les annonces autour de vous';
      if(err === 'Timeout') error = 'La localisation a pris trop de temps';
      Toast.show({
        type: 'error',
        text1: 'Erreur Localisation',
        text2: JSON.stringify(error) || 'Erreur lors de la récupération de la localisation',
      });
    }
  };

  //update user location in database
  const updateUserLocation = async () => {
    console.log('[UPDATE LOCATION]')
    UsersAPI.updateLocation(currentUser?.location)
      .then((response: any) => {
      })
      .catch((err: any) => {
        console.error('Error updating user location:', err);
      });
  };

  const fetchBoundingBox = async () => {
    try {
      const bbox = await getBoundingBox(location?.latitude, location?.longitude);
      console.log('Bounding box:', bbox);
      const mapUrl = `https://www.openstreetmap.org/export/embed.html?bbox=${bbox?.minLong}%2C${bbox?.minLat}%2C${bbox?.maxLong}%2C${bbox?.maxLat}&layer=mapnik&marker=${location?.latitude},${location?.longitude}`;
      console.log('Map URL:', mapUrl);
    } catch (err) {
      console.error('Error getting bounding box:', err);
    }
  };

  return (
    <Background>
      <Styling.CustomView>
        <AppTitle /> 
          
          <SlidingTextBanner />
         
          <View style={{
            borderWidth: 1,
            borderColor: '#ccc',
            width: '100%',
            borderRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
            marginTop: 20,
          }}>
            <Text style={{
              fontSize: 20,
              fontWeight: 'bold',
              textAlign: 'center',
              marginBottom: 10,
            }}>Bonjour, {currentUser?.name} {currentUser?.lastName} 👋 </Text>
            <MapView
              style={styles.map}
              initialRegion={initialRegion}
              region={initialRegion}
            >
            <Marker
              coordinate={{ latitude: initialRegion.latitude, longitude: initialRegion.longitude }}
              title="Ma position"
              description=""
            />
          </MapView>

          <View style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            alignSelf: 'flex-start',
            gap: 10,
            marginTop: 10,
            paddingHorizontal: 10,
          }}>
            <TouchableOpacity onPress={() => fetchLocationAndAddress()}>
              <LocationIcon 
                size={30} 
                color={(address?.road || address?.city || address?.postalCode || address?.country) ? '#404ad5' : '#ff0000'}
              />
            </TouchableOpacity>
            {!address?.road && !address?.postalCode && !address?.city && !address?.country?
            <Text style={{
              fontSize: 18,
              fontWeight: 'semibold',
              color: '#fff',
              width: "90%",
            }}>
              Non disponible
            </Text>
            :
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#fff', width: "90%"}}>
              {address?.road} {address?.road? ', ': ''} {address?.postalCode} {address?.city} {address?.country}
            </Text>
            }

          </View>

          </View>
          
    <ScrollView>
      <Styling.ItemsContainer>
        <Text style={{fontSize: 20, fontWeight: "bold"}}>Favoris: </Text>
        <WishList />
      </Styling.ItemsContainer>
    </ScrollView>
      </Styling.CustomView>
    </Background>
  );
};

const styles = StyleSheet.create({
  map: {
    width: '100%',
    height: 150,
  },
});
export default LandingScreen;
