import React, { useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Text, TouchableOpacity, TextInput, SafeAreaView, ScrollView } from 'react-native';
import { useActiveScreen } from '../../context/ScreenContext';

import Background from '../../components/layers/Background';
import AppTitle from '../../components/shared/AppTitle';
import SignIn from '../../components/auth/SignIn';


const AuthScreen = () => {
  const { setActiveScreen } = useActiveScreen();
  const navigation = useNavigation();


  return (
    <Background>
      <AppTitle/>
      <View style={{ 
          flex: 1, 
          justifyContent: 'center', 
          alignItems: 'center',
      }}>
        <SignIn />

        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <Text>Pas encore inscrit ? </Text>
          <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen' as never)}>
          <Text style={{fontWeight: 'bold', marginTop: 2}}>Inscription</Text> 
          </TouchableOpacity>
        </View>

      </View>
    </Background>
  );
};

export default AuthScreen;