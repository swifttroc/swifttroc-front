import { Button, Modal, StyleSheet, Text, TextInput, TouchableOpacity, View, ScrollView, Image, Alert } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import Background from '../../components/layers/Background';
import ProfilCard from '../../components/shared/ProfilCard';
import SettingsMenu from '../../components/SettingsMenu';
import WishList from '../../components/shared/WishList'
import { NavigationProp, useFocusEffect, useNavigation } from '@react-navigation/native';
import { ObjectsAPI } from '../../API/ObjectsAPI';
import { UsersAPI } from '../../API/UsersAPI';
import ObjectCard from '../../components/shared/ObjectCard';
import { useActiveScreen } from '../../context/ScreenContext';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { RootStackParamList } from '../../components/layers/AppNavigator';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import TradesList from '../../components/shared/TradesList';
import { TradesAPI } from '../../API/TradesAPI';
import Toast from 'react-native-toast-message';

const ProfilScreen = () => {
    const [activeMenu, setActiveMenu] = useState("");
    const [objects, setObjects] = useState([]);
    const [trades, setTrades] = useState([]);
    const [isModalVisible, setModalVisible] = useState(false);
    const { currentUser, setCurrentUser } = useActiveScreen();
    const [user, setUser] = useState({
        name: '',
        lastName: '',
        pseudo: '',
        avatar: '',
        email: '',
        phone: ''
    });
    const [imageUri, setImageUri] = useState<string | null>(null); // Pour stocker l'image sélectionnée

    useEffect(() => {
        if (isModalVisible && currentUser) {
            setUser({
                name: currentUser.name || '',
                lastName: currentUser.lastName || '',
                pseudo: currentUser.pseudo || '',
                avatar: currentUser.avatar || '',
                email: currentUser.email || '',
                phone: currentUser.phone || ''
            });
            setImageUri(currentUser.avatar); // Récupérer l'URL de l'avatar si disponible
        }
    }, [isModalVisible, currentUser]);

    useFocusEffect(
        useCallback(() => {
            setActiveMenu("");
            getAuthObjects();
            getAuthTrades();
        }, [])
    );

    const getAuthObjects = async () => {
        ObjectsAPI.getMyObjects()
        .then((response: any) => {
            setObjects(response.data);
        })
        .catch((error: any) => {
            Toast.show({
                type: 'error',
                text1: 'Erreur',
                text2: 'Une erreur est survenue lors de la récupération de vos objets.'
            })
        });
};

    const getAuthTrades = async () => {
        TradesAPI.GET()
        .then((response: any) => {
            setTrades(response.data);
        })
        .catch((error: any) => {
            console.log(error);
        });
    };

    const handleSave = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const formData = new FormData();

            formData.append('name', user.name);
            formData.append('lastName', user.lastName);
            formData.append('pseudo', user.pseudo);
            if (imageUri) {
                const filename = imageUri.split('/').pop();
                const imageFile: any = {
                    uri: imageUri,
                    name: filename,
                    type: 'image/jpeg',
                };
                formData.append('avatar', imageFile); // Ajouter l'image à formData
            }

            const response = await UsersAPI.updateUser(token || '', formData); 

            if (response && response._id) {
                setCurrentUser({
                    ...currentUser,
                    name: response.name,
                    lastName: response.lastName,
                    pseudo: response.pseudo,
                    avatar: response.avatar
                });
                setModalVisible(false);
            } else {
                console.log('Error updating user:', response.data);
            }
        } catch (error) {
            console.error('An error occurred while updating the user:', error);
        }
    };

    const handleImagePicker = () => {
        Alert.alert(
            "Sélectionner une image",
            "Choisissez une option",
            [
                { text: "Annuler", style: "cancel" },
                {
                    text: "Prendre une photo",
                    onPress: () => launchCamera(
                        { mediaType: 'photo', cameraType: 'back', saveToPhotos: true },
                        (response) => {
                            if (response.assets && response.assets.length > 0) {
                                setImageUri(response.assets[0].uri || null); // Met à jour l'URI de l'image
                            }
                        }
                    )
                },
                {
                    text: "Choisir dans la galerie",
                    onPress: () => launchImageLibrary(
                        { mediaType: 'photo', selectionLimit: 1 },
                        (response) => {
                            if (response.assets && response.assets.length > 0) {
                                setImageUri(response.assets[0].uri || null); // Met à jour l'URI de l'image
                            }
                        }
                    )
                }
            ]
        );
    };

    const menuRoutes = () => {
        switch (activeMenu) {
            case "Mes objets":
                return <CardList objects={objects} />;
            case "Favoris":
                return <WishList />;
            case "Demandes":
                return <TradesList />;
            default:
                return <SettingsMenu />;
        }
    };

    useEffect(() => {
        if(activeMenu === "Demandes") getAuthTrades();
        if(activeMenu === "Mes objets") getAuthObjects();
    }, [activeMenu]);

    return (
        <Background>
            <View style={{ justifyContent: 'center', alignItems: 'center', padding: 15 }}>
                <ProfilCard
                    activeMenu={activeMenu}
                    setActiveMenu={setActiveMenu}
                    objectsLength={objects.length}
                    tradesLength={trades.length}
                    onEditProfile={() => setModalVisible(true)} // Passer la fonction pour ouvrir la modal
                />
                <ScrollView style={{ marginTop: 20, width: '100%', height: '40%' }}>
                    {menuRoutes()}
                </ScrollView>
            </View>

            {/* Modal pour modifier le profil */}
            <Modal
                visible={isModalVisible}
                transparent={true}
                animationType="slide"
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modalContent}>
                        <Text style={styles.modalTitle}>Modifier le profil</Text>

                        <TouchableOpacity onPress={handleImagePicker} style={styles.imagePicker}>
                            {imageUri ? (
                                <Image source={{ uri: imageUri }} style={styles.imagePreview} />
                            ) : (
                                <Text style={styles.imagePlaceholder}>Choisir un avatar</Text>
                            )}
                        </TouchableOpacity>

                        <TextInput
                            style={styles.input}
                            placeholder="Nom"
                            value={user.name}
                            onChangeText={(text) => setUser({ ...user, name: text })}
                        />
                        <TextInput
                            style={styles.input}
                            placeholder="Prénom"
                            value={user.lastName}
                            onChangeText={(text) => setUser({ ...user, lastName: text })}
                        />
                        <TextInput
                            style={styles.input}
                            placeholder="Pseudo"
                            value={user.pseudo}
                            onChangeText={(text) => setUser({ ...user, pseudo: text })}
                        />
                        <TextInput
                            style={[styles.input, styles.disabledInput]}
                            placeholder="Email"
                            value={user.email}
                            editable={false}
                        />
                        <TextInput
                            style={[styles.input, styles.disabledInput]}
                            placeholder="Téléphone"
                            value={user.phone}
                            editable={false}
                        />
                        <Button title="Enregistrer" onPress={handleSave} />
                        <Button title="Annuler" onPress={() => setModalVisible(false)} color="red" />
                    </View>
                </View>
            </Modal>
        </Background>
    );
};

const CardList = ({ objects }: { objects: any }) => {
    const { wishlist } = useActiveScreen();
    const navigation = useNavigation<NavigationProp<RootStackParamList>>();

    return (
        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', marginTop: 20 }}>
            {objects?.map((object: any) => (
                <TouchableOpacity
                    onPress={() => navigation.navigate('DetailScreen', { object })}
                    key={object._id}
                    style={{ width: '45%', margin: 5, height: 260 }}
                >
                    <ObjectCard object={object} size="small" />
                </TouchableOpacity>
            ))}
        </View>
    );
};

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalContent: {
        width: '80%',
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
    },
    modalTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 20,
        textAlign: 'center',
    },
    input: {
        color: '#000',
        height: 40,
        borderColor: '#ccc',
        borderWidth: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        borderRadius: 5,
    },
    disabledInput: {
        backgroundColor: '#eee',
    },
    imagePicker: {
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        height: 150,
    },
    imagePreview: {
        width: 150,
        height: 150,
        borderRadius: 75,
    },
    imagePlaceholder: {
        color: '#aaa',
        fontSize: 16,
    },
});

export default ProfilScreen;
