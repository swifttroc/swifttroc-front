import axios from 'axios';

import { API_URL } from '@env';
import { getToken } from '../utils/function';
const URL = API_URL + '/object/';

export const ObjectsAPI = {
    GET: async () => {
        try {
            const objects = await axios.get(URL);
            return objects.data;
        } catch (error) {
            console.log(error);
            return error;
        }
    },
    // Récupération des objets avec filtres
    //Add localisation and radius to searchParams
    GET_FILTERED: async (searchParams: { 
        title?: string; 
        description?: string; 
        category?: string; 
        localisation?: { latitude: number; longitude: number }; 
        radius?: number 
    }) => {
        try {
            const token = await getToken();
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                }
            }

            const response = await axios.get(`${URL}filtered`, {
                params: {
                    ...searchParams,
                    localisation: searchParams.localisation 
                        ? `${searchParams.localisation.latitude},${searchParams.localisation.longitude}` 
                        : undefined,
                    radius: searchParams.radius,
                },
                ...config
            });
            return response.data;
        } catch (error) {
            console.log(error);
            return error;
        }
    },
    POST: async (object: FormData) => {
        try {
            const token = await getToken();
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                    'Content-Type': 'multipart/form-data'
                }
            }
            const response = await axios.post(URL, object, config);
            return response;
        } catch (error: any) {
            return error?.response;
        }
    },

    getMyObjects: async () => {
        try {
            const token = await getToken();
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                }
            }
            const response = await axios.get(URL + 'me', config);
            return response;
        } catch (error: any) {
            return error?.response;
        }
    },

    getSpecificUserObjects: async (id: string) => {
        try {
            const response = await axios.get(`${URL}user-specific/` + id);
            return response;
        } catch (error: any) {
            return error?.response;
        }
    }

}
