import axios from 'axios';

import { API_URL } from '@env';
import { getToken } from '../utils/function';
const URL = API_URL + '/user/';

export const UsersAPI = {
   
    Login: async (user: any) => {
        try {
            console.log('URL', URL + 'login');
            const response = await axios.post(URL + 'login', user);
            return response;
        } catch (error: any) {
            return error.response;
        }
    },
    Register: async (user: any) => {
        try {
            const response = await axios.post(URL + 'register', user);
            console.log('data from the API service', response)
            return response;
        } catch (error: any) {
            return error.response;
        }
    },
    startRegistration: async (user: any) => {
        try {
            const response = await axios.post(URL + 'start-registration', user);
            return response;
        } catch (error: any) {
            return error.response;
        }
    },
    verifyPhone: async (verificationCode: string) => {
        try {
            const response = await axios.post(URL + 'verify-phone', {
                verificationCode,
            });
            return response;
        } catch (error: any) {
            return error.response;
        }
    },

    getUser: async (token: string) => {
        try {
            const response = await axios.get(URL + 'me', {
                headers: {
                    'auth-token': token,
                },
            });
            return response.data;
        } catch (error: any) {
            return error.response;
        }
    },

    getuserById: async (id: string) => {
        try {
            const response = await axios.get(URL + id);
            return response;
        } catch (error: any) {
            return error.response
        }
    },

    updateUser: async (token: string, object: FormData) => {
        try {
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                    'Content-Type': 'multipart/form-data'
                }
            }
            const response = await axios.put(URL + 'updateUser', object, config);
            console.log('response from the API service', response.data);
            return response.data;
        } catch (error: any) {
            return error.response;
        }
    },

    updateLocation: async (location: any) => {
       try {
            const token = await getToken();
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                }
            }

            const response = await axios.put(URL + 'updateLocation', location, config);
            return response;
            
       } catch (error: any) {
            return error.response;
       }
    }
}