import axios from 'axios';

import { API_URL } from '@env';
import { getToken } from '../utils/function';
const URL = API_URL + '/trade/';

export const TradesAPI = {
    GET: async () => {
        try {
            const token = await getToken();
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                }
            }
            const response = await axios.get(URL, config);
            return response;
        } catch (error) {
            console.log(error);
            return error;
        }
    },
    
    POST: async (trade: any) => {
        try {
            const token = await getToken();
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                }
            }
            const response = await axios.post(URL, trade, config);
            return response;
        } catch (error: any) {
            return error?.response;
        }
    },

    PUT: async (tradeId: string, status: string) => {
        try {
            const token = await getToken();
            if(!token) return {error: 'No token'};

            const config = {
                headers: {
                    'auth-token': token,
                }
            }
            const response = await axios.put(URL, {tradeId, status}, config);
            return response;
        } catch (error: any) {
            return error?.response;
        }
    },

    PUT_BOOLEAN: async (tradeId: string, field: string, value: boolean) => {
        console.log('HERE')
        try {
            const token = await getToken();
            if (!token) return { error: 'No token' };
    
            const config = {
                headers: {
                    'auth-token': token,
                },
            };
    
            // On envoie la requête avec le tradeId, le champ (par ex: proposerAcceptedTrade), et la nouvelle valeur
            const response = await axios.put(`${URL}${tradeId}/boolean`, { field, value }, config);
           
            return response;
        } catch (error: any) {
            return error?.response;
        }
    },
    
    

}
