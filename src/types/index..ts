export type Object = {
    _id: string;
    title: string;
    description: string;
    categories: string[];
    images: string[];
    publicationDate: Date;
    owner: string;
    isTraded: boolean;
};

