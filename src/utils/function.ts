import AsyncStorage from "@react-native-async-storage/async-storage"
import { UsersAPI } from "../API/UsersAPI"
import Geolocation from "@react-native-community/geolocation"
import { Platform, PermissionsAndroid } from 'react-native';

export const getToken = async () => {
    try {
        const token = await AsyncStorage.getItem('token')
        return token
    } catch (error) {
        console.log('Error getting token: ', error)
        return null
    }
}

export const getUser = async () => {
    const token = await getToken()
    if(!token) return null

    try {
        const response = UsersAPI.getUser(token)
        return response;
    } catch (error) {
        console.log('Error getting user: ', error)
        return null
    }
}

export const capitalizeFirstLetter = (text: string) => {
    return text?.charAt(0).toUpperCase() + text?.slice(1);
};

export const getPhoneLocation = async () => {
    try {
        if (Platform.OS === 'android') {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Location Permission',
                    message: 'Cette application a besoin de votre permission pour accéder à votre position',
                    buttonNeutral: 'Demande plus tard',
                    buttonNegative: 'Annuler',
                    buttonPositive: 'Accepter',
                }
            );

            if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
                console.log('Location permission denied');
                throw new Error('Location permission denied');

            }
            console.log('Location permission granted');
        }

        return await getLocationWithRetries();
        // return await getLocation();
    } catch (err) {
        console.warn('Error getting phone location:', err);
        throw "Position unavailable";
    }
};

const getLocationWithRetries = (retries = 1): Promise<{ latitude: number; longitude: number }> => {
    return new Promise((resolve, reject) => {
        const attemptLocation = (attemptNumber: number) => {
            Geolocation.getCurrentPosition(
                (position) => {
                    const location = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    };
                    console.log(`Attempt ${attemptNumber} succeeded:`, location);
                    resolve(location);
                },
                (error) => {
                    console.log(`Attempt ${attemptNumber} failed:`, error.message);
                    if (attemptNumber < retries) {
                        console.log(`Retrying... (${attemptNumber + 1}/${retries})`);
                        attemptLocation(attemptNumber + 1);
                    } else {
                        reject(error);
                    }
                },
                {
                    enableHighAccuracy: false,
                    timeout: 20000,  // Slightly longer timeout
                    // maximumAge: 0,  // Prevent returning cached location
                }
            );
        };

        attemptLocation(1);
    });
};

const getLocation = () => {
    return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(
            (position: any) => {
                const location = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                };
                resolve(location);
            },
            (error) => {
                let errorMessage = 'Unknown error';
                switch (error.code) {
                  case 1:
                    errorMessage = 'Permission denied';
                    break;
                  case 2:
                    errorMessage = 'Position unavailable';
                    break;
                  case 3:
                    errorMessage = 'Timeout';
                    break;
                  default:
                    errorMessage = error.message;
                }
                console.log('Error getting location:', errorMessage);
                reject(errorMessage)
            },
            //cause trop souvent des bugs
            // { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    });
};

  //fonction qui transforme les coordonnées en adresse
  export const getAddressFromLocation = async (location : any) => {
    try {
        const response = await fetch(`https://nominatim.openstreetmap.org/reverse?lat=${location.latitude}&lon=${location.longitude}&format=json`);
        const data = await response.json();
        const addressComponents = {
            road: data.address.road,
            postalCode: data.address.postcode,
            city: data.address.town,
            country: data.address.country,
            
          };
      
          return addressComponents;
    } catch (error) {
        console.log('Error fetching address:', error);
        throw error;
    }
};

//fonction qui calcule la distance entre deux points
export function calculateDistance(lat1: number, lon1: number, lat2: number, lon2: number): number {
    // Radius of the Earth in kilometers
    const R = 6371;

    // Convert degrees to radians
    const toRadians = (degrees: number): number => degrees * (Math.PI / 180);

    const dLat = toRadians(lat2 - lat1);
    const dLon = toRadians(lon2 - lon1);

    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(toRadians(lat1)) * Math.cos(toRadians(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const distance = R * c;

    return distance;
}

export async function getBoundingBox(lat : any, long : any, distanceKm = 5) {
    const earthRadiusKm = 6371;
    const latOffset = distanceKm / 111.32;
    const longOffset = distanceKm / (111.32 * Math.cos(lat * Math.PI / 180));
const boundingCoordinates = {
     minLat : lat - latOffset,
     maxLat : lat + latOffset,
     minLong :long - longOffset,
     maxLong :long + longOffset,
}
    return boundingCoordinates;
}
