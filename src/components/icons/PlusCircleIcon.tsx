import Svg, { Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color: string;
}

const PlusCircleIcon = ({ color, size } : IconProps ) => {
  return (
    <Svg
        width={size}
        height={size}
        viewBox="0 0 26 26"
        fill="none"
    >
        <Path
        d="M17.334 14.084H8.666a1.084 1.084 0 110-2.168h8.666a1.084 1.084 0 01.002 2.168z"
        fill="#37B6E9"
        />
        <Path
        d="M13 26C5.832 26 0 20.168 0 13S5.832 0 13 0s13 5.832 13 13-5.832 13-13 13zm0-23.834C7.026 2.166 2.166 7.026 2.166 13c0 5.974 4.86 10.834 10.834 10.834 5.974 0 10.834-4.86 10.834-10.834 0-5.974-4.86-10.834-10.834-10.834z"
        fill={color}
        fillOpacity={0.6}
        />
        <Path
        d="M13 18.416c-.6 0-1.084-.485-1.084-1.085V8.666a1.084 1.084 0 112.168 0v8.665c0 .6-.485 1.085-1.084 1.085z"
        fill="#37B6E9"
        />
  </Svg>
  )
}

export default PlusCircleIcon