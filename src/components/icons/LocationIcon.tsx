import Svg, { Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

const LocationIcon = ({ color, size } : IconProps ) => {
  return (
    <Svg
    width={size}
    height={size}
    viewBox="0 0 26 26"
    fill="none"
  >
    <Path
      d="M13 0C7.616 0 3.25 4.366 3.25 9.75c0 2.12.695 4.066 1.85 5.66.021.037.024.08.048.117l6.5 9.75a1.625 1.625 0 002.704 0l6.5-9.75c.024-.037.027-.08.047-.118A9.598 9.598 0 0022.75 9.75C22.75 4.366 18.384 0 13 0zm0 13a3.25 3.25 0 110-6.5 3.25 3.25 0 010 6.5z"
      fill={color || "#4B4CED"}
      fillOpacity={0.8}
    />
  </Svg>
  )
}

export default LocationIcon