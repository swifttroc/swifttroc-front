import Svg, { ClipPath, Defs, G, Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

const CheckIcon = ({ color, size } : IconProps ) => {
  return (
  <Svg
    width={size}
    height={size}
    viewBox="0 0 20 20"
    fill="none"
  >
    <G clipPath="url(#clip0_15_185)">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10 20a10 10 0 110-20 10 10 0 010 20zm-5.662-7.745a1.161 1.161 0 010-1.633l.589-.586a1.062 1.062 0 011.524.017l1.04 1.08a.53.53 0 00.77 0l5.3-5.464a1.062 1.062 0 011.534-.009l.572.578a1.16 1.16 0 010 1.615l-7.037 7.15a1.063 1.063 0 01-1.517.01l-2.775-2.758z"
        fill={color || "#699F4C"}
      />
    </G>
    <Defs>
      <ClipPath id="clip0_15_185">
        <Path fill="#fff" d="M0 0H20V20H0z" />
      </ClipPath>
    </Defs>
  </Svg>
  )
}

export default CheckIcon