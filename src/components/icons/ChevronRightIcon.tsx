import Svg, { Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

const ChevronRightIcon = ({ color, size } : IconProps ) => {
  return (
    <Svg
      width={size}
      height={size}
      viewBox="0 0 24 24"
      fill="none"
    >
      <Path
        d="M9.5 7l5 5-5 5"
        stroke={color || '#000'}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default ChevronRightIcon