import Svg, { Circle, Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

const FilterIcon = ({ color, size } : IconProps ) => {
  return (
    <Svg
    width={size}
    height={size}
    viewBox="0 0 64 64"
    strokeWidth={3}
    stroke={color || "#000"}
    fill="none"
  >
    <Path d="M50.69 32L56.32 32" />
    <Path d="M7.68 32L38.69 32" />
    <Path d="M26.54 15.97L56.32 15.97" />
    <Path d="M7.68 15.97L14.56 15.97" />
    <Path d="M35 48.03L56.32 48.03" />
    <Path d="M7.68 48.03L23 48.03" />
    <Circle cx={20.55} cy={15.66} r={6} />
    <Circle cx={44.69} cy={32} r={6} />
    <Circle cx={29} cy={48.03} r={6} />
  </Svg>
  )
}

export default FilterIcon