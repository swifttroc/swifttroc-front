import Svg, { Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

const ColorPaletteIcon = ({ color, size } : IconProps ) => {
  return (
  <Svg
    width={size}
    height={size}
    viewBox="0 0 20 20"
    fill="none"
  >
    <Path
      d="M10.23 1.428c-3.687 0-7.9 2.124-7.9 8.12 0 4.355 3.272 9.023 8.276 9.023h.075c1.88-.034 3.902-1.25 3.902-3.84 0-.495-.184-.925-.346-1.303a6.886 6.886 0 01-.124-.297c-.37-.933.038-1.274.998-1.957 1.088-.774 2.577-1.833 2.558-4.637 0-1.666-1.818-5.109-7.44-5.109zm.24 15.086c-.961 0-1.716-.754-1.716-1.714s.755-1.714 1.715-1.714c.96 0 1.714.754 1.714 1.714s-.754 1.714-1.714 1.714z"
      fill="#FFC107"
    />
    <Path d="M14.455 8.286a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" fill="#9C27B0" />
    <Path d="M10.598 5.714a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" fill="#2196F3" />
    <Path d="M6.312 7.857a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" fill="#4CAF50" />
    <Path d="M5.884 12.571a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" fill="#FF3D00" />
  </Svg>
  )
}

export default ColorPaletteIcon