import Svg, { Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

const PinIcon = ({ color, size } : IconProps ) => {
  return (
  <Svg
    width={size}
    height={size*1.8}
    viewBox="0 0 10 18"
    fill="none"
  >
    <Path
      d="M4.167 8.97A4.168 4.168 0 015 .72a4.167 4.167 0 01.833 8.25v7.584a.833.833 0 01-1.666 0V8.971z"
      fill="#37B6E9"
    />
  </Svg>
  )
}

export default PinIcon