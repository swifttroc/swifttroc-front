import Svg, { ClipPath, Defs, G, Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

export const LockPasswordIcon = ({ color, size } : IconProps ) => {
  return (
  <Svg
    width={size}
    height={size}
    viewBox="0 0 24 24"
    fill="none"
  >
    <Path
      opacity={0.5}
      d="M2 16c0-2.828 0-4.243.879-5.121C3.757 10 5.172 10 8 10h8c2.828 0 4.243 0 5.121.879C22 11.757 22 13.172 22 16c0 2.828 0 4.243-.879 5.121C20.243 22 18.828 22 16 22H8c-2.828 0-4.243 0-5.121-.879C2 20.243 2 18.828 2 16z"
      fill={color || "#1C274C"}
    />
    <Path
      d="M8 17a1 1 0 100-2 1 1 0 000 2zM12 17a1 1 0 100-2 1 1 0 000 2zM17 16a1 1 0 11-2 0 1 1 0 012 0zM6.75 8a5.25 5.25 0 0110.5 0v2.004c.567.005 1.064.018 1.5.05V8a6.75 6.75 0 00-13.5 0v2.055a23.57 23.57 0 011.5-.051V8z"
      fill={color || "#1C274C"}
    />
  </Svg>
  )
}

export const UnlockPasswordIcon = ({ color, size } : IconProps ) => {
  return (
  <Svg
    width={size}
    height={size}
    viewBox="0 0 24 24"
    fill="none"
  >
    <Path
      opacity={0.5}
      d="M2 16c0-2.828 0-4.243.879-5.121C3.757 10 5.172 10 8 10h8c2.828 0 4.243 0 5.121.879C22 11.757 22 13.172 22 16c0 2.828 0 4.243-.879 5.121C20.243 22 18.828 22 16 22H8c-2.828 0-4.243 0-5.121-.879C2 20.243 2 18.828 2 16z"
      fill={color || "#1C274C"}
    />
    <Path
      d="M8 17a1 1 0 100-2 1 1 0 000 2zM12 17a1 1 0 100-2 1 1 0 000 2zM17 16a1 1 0 11-2 0 1 1 0 012 0zM6.75 8a5.25 5.25 0 0110.335-1.313.75.75 0 001.452-.374A6.75 6.75 0 005.25 8v2.055a23.57 23.57 0 011.5-.051V8z"
      fill={color || "#1C274C"}
    />
  </Svg>
  )
}
