import Svg, { G, Mask, Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color: string;
}

const SearchIcon = ({ color, size } : IconProps ) => {
  return (
    <Svg
    width={size}
    height={size}
    viewBox="0 0 22 22"
    fill="none"
  >
    <Path
      d="M13.77 2.787c-2.428.777-4.02 2.688-4.778 5.733-1.136 4.567-2.544 5.15-7.129 3.525 1.647 3.041 4.984 4.26 7.129 4.26 2.145 0 7.701-2.986 7.065-8.664a23.485 23.485 0 00-2.288-4.854z"
      fill="#D8D8D8"
    />
    <Mask
      id="a"
      maskUnits="userSpaceOnUse"
      x={1}
      y={2}
      width={16}
      height={15}
    >
      <Path
        d="M13.77 2.787c-2.428.777-4.02 2.688-4.778 5.733-1.136 4.567-2.544 5.15-7.129 3.525 1.647 3.041 4.984 4.26 7.129 4.26 2.145 0 7.701-2.986 7.065-8.664a23.485 23.485 0 00-2.288-4.854z"
        fill="#fff"
      />
    </Mask>
    <G mask="url(#a)">
      <Path d="M23.833-2.167h-26v26h26v-26z" fill="#37B6E9" />
    </G>
    <Path
      d="M15.36 13.834l5.993 6.002a1.072 1.072 0 11-1.517 1.517l-5.99-6a8.58 8.58 0 111.515-1.518zm-6.78 1.18a6.434 6.434 0 100-12.87 6.434 6.434 0 000 12.87z"
      fill="#000"
    />
    <Mask
      id="b"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={22}
      height={22}
    >
      <Path
        d="M15.36 13.834l5.993 6.002a1.072 1.072 0 11-1.517 1.517l-5.99-6a8.58 8.58 0 111.515-1.518zm-6.78 1.18a6.434 6.434 0 100-12.87 6.434 6.434 0 000 12.87z"
        fill={color}
      />
    </Mask>
    <G mask="url(#b)">
      <Path d="M23.833-2.167h-26v26h26v-26z" fill={color} fillOpacity={0.6} />
    </G>
  </Svg>
  )
}

export default SearchIcon