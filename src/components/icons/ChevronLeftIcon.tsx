import Svg, { Path } from 'react-native-svg';


interface IconProps {
  size: number;
  color?: string;
}

const ChevronLeftIcon = ({ color, size } : IconProps ) => {
  return (
    <Svg
      width={size}
      height={size}
      viewBox="0 0 24 24"
      fill="none"
    >
      <Path
        d="M14.5 17l-5-5 5-5"
        stroke={color || '#000'}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default ChevronLeftIcon