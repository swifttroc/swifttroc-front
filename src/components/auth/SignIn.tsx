import { View, Text, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import TextField from '../shared/TextField'
import { UsersAPI } from '../../API/UsersAPI'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import Toast from 'react-native-toast-message'
import { useActiveScreen } from '../../context/ScreenContext'
import { LockPasswordIcon, UnlockPasswordIcon } from '../icons/PasswordIcons'

const SignIn = () => {

    const navigation = useNavigation()
    const { setCurrentUser } = useActiveScreen()
    const [showPassword, setShowPassword] = useState(false)
    const [user, setUser] = useState({
        identifier: '',
        password: ''
    })

    const handleChange = (name: string, value: string) => {
        setUser(prevState => ({
            ...prevState,
            [name]: value
        }))
    }
    
    const handleSubmit = async () => {
        const form = {
            password: user.password,
            email: '',
            phone: ''
        }

        //remove blank spaces
        user.identifier = user.identifier.replace(/\s/g, '')

        const isEmail = user.identifier.includes('@')
        const isPhone = !isNaN(Number(user.identifier)) || user.identifier[0] === '+' && !isNaN(Number(user.identifier.slice(1)))
        
        if (isEmail) {
            form.email = user.identifier
        } else if (isPhone) {
            form.phone = user.identifier
        } else {
            Toast.show({
                type: 'error',
                text1: 'Invalid email or phone number',
                text2: 'Please enter a valid email or phone number'
            })
            return
        }

        UsersAPI.Login(form)
        .then(async (response) => {
            if (response.status === 200) {
                Toast.show({
                    type: 'success',
                    text1: 'Bienvenue',
                })
                const token = response.data.token
                await AsyncStorage.setItem('token', token)
                setUser({
                    identifier: '',
                    password: ''
                })
                setCurrentUser(response.data.user)
                navigation.navigate('Main' as never)
            }
            else {
                Toast.show({
                    type: 'error',
                    text1: 'Erreur de connexion',
                    text2: response?.data?.message || 'Une erreur est survenue'
                })
            }
        })
        .catch((error) => {
            console.log('[ERROR_LOGIN]: ', error?.response?.data)
            Toast.show({
                type: 'error',
                text1: 'Erreur de connexion',
                text2: 'Une erreur est survenue'
            })
        })
    }

  return (
   
    <View style={{
        borderWidth: 1.5, 
        borderRadius: 20,
        borderColor: 'white',
        width: '90%',
        height: '70%',
        backgroundColor: "transparent",
        flexDirection: 'column',
        justifyContent: 'space-around',
    }}>
      <Text style={{ 
          color: 'white', 
          fontSize: 30, 
          textAlign: 'center',
          textTransform: 'uppercase',
          fontWeight: 'bold',
          margin: 20,
      }}>
        Login
      </Text>
      <View>
        <TextField
            type="text" 
            placeholder="Email ou Téléphone" 
            value={user.identifier}
            onChangeText={(text) => handleChange('identifier', text)}
        />
        <View style={{
            position: 'relative',
            alignItems: 'center',
        }}>
            <TextField
                type={showPassword ? 'text' : 'password'}
                placeholder="Mot de passe" 
                value={user.password}
                onChangeText={(text) => handleChange('password', text)}
            />
            <TouchableOpacity onPress={() => setShowPassword(!showPassword)} style={{
                position: 'absolute',
                left: 30,
                top: 32,
                zIndex: 1,
            }}>
                {showPassword ? 
                    <UnlockPasswordIcon size={28} color='#fff'/> 
                    : 
                    <LockPasswordIcon size={28} color='#fff'/>
                }
            </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        onPress={handleSubmit}
        style={{
          borderColor: 'white',
          borderWidth: 1.5,
          backgroundColor: '#62a7f0',
          borderRadius: 18,
          margin: 20,
          padding: 10,
          width: '54%',
          alignSelf: 'center',
        }}
      >
        <Text style={{textAlign: "center", fontWeight: 'bold', fontSize: 16}}>Connexion</Text> 
      </TouchableOpacity>
    </View>
  )
}

export default SignIn