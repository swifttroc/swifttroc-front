import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import React from 'react'
import SettingIcon from './icons/SettingIcon'
import CheckIcon from './icons/CheckIcon'
import NotificationIcon from './icons/NotificationIcon'
import ColorPaletteIcon from './icons/ColorPaletteIcon'
import ChevronRightIcon from './icons/ChevronRightIcon'

const SettingsMenu = () => {

    const settings = [
        {
            title: "Notifications",
            icon: <NotificationIcon size={26} />,
            onPress: () => {},
        },
        {
            title: "Autorisations",
            icon: <CheckIcon size={26}/>,
            onPress: () => {},
        },
        {
            title: "Thème et Couleurs",
            icon: <ColorPaletteIcon size={26} />,
            onPress: () => {},
        },
        
    ]
  return (
    <View>
        <View style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        }}>
            <SettingIcon size={26} />
            <Text style={{fontWeight: 'bold', color: "#fff", fontSize: 16}}> Paramètres </Text>
        </View>

        <ScrollView style={{
            marginTop: 20,
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
        }}>
            {settings.map((setting, index) => (
                <TouchableOpacity onPress={setting.onPress} key={index} style={{
                    backgroundColor: '#fff',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    padding: 10,
                    width: '90%',
                    borderRadius: 10,
                    marginVertical: 10,
                    alignSelf: index % 2 === 0 ? 'flex-start' : 'flex-end',
                }}>
                    <View style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        {setting.icon}
                        <Text style={{color: '#000', fontSize: 16, fontWeight: 'bold', marginLeft: 10}}>{setting.title}</Text>
                    </View>
                    <ChevronRightIcon size={26} />
                </TouchableOpacity>
            ))}

        </ScrollView>
    </View>
  )
}

export default SettingsMenu