import React, { useState, useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TouchableOpacity, Animated, Easing, View } from 'react-native';

// Screens
import HomeScreen from '../../screens/HomeScreen/HomeScreen';
import SwipeScreen from '../../screens/SwipeScreen/SwipeScreen';
import AddObjectScreen from '../../screens/AddObjectScreen/AddObjectScreen';
import SearchScreen from '../../screens/SearchScreen/SearchScreen';

// Icons
import AvatarIcons from '../shared/AvatarIcons';
import HomeIcon from '../icons/HomeIcon';
import SwipeIcon from '../icons/SwipeIcon';
import PlusCircleIcon from '../icons/PlusCircleIcon';
import SearchIcon from '../icons/SearchIcon';
import { useNavigation } from '@react-navigation/native';
import LinearGradientBackground from './LinearGradientBackground';
import { useActiveScreen } from '../../context/ScreenContext';
import CustomHeader from './CustomHeader';
import ProfilScreen from '../../screens/ProfilScreen/ProfilScreen';

const Tab = createBottomTabNavigator();

const BottomNavBar: React.FC = () => {
  const navigation = useNavigation();
  const [activeIcon, setActiveIcon] = useState('home');
  const { activeScreen, setActiveScreen, currentUser } = useActiveScreen();

  // Animated values for translateY animation for each icon
  const homeTranslateYAnim = useState(new Animated.Value(0))[0];
  const swipeTranslateYAnim = useState(new Animated.Value(0))[0];
  const addObjectTranslateYAnim = useState(new Animated.Value(0))[0];
  const searchTranslateYAnim = useState(new Animated.Value(0))[0];
  const authTranslateYAnim = useState(new Animated.Value(0))[0];

  const handleIconPress = (iconName: string) => {
    handleSetActiveScreen(iconName);
    setActiveIcon(iconName);
  };

  const handleSetActiveScreen = (iconName: string) => {
    switch (iconName) {
      case 'home':
        setActiveScreen('HomeScreen');
        break;
      case 'swipe':
        setActiveScreen('SwipeScreen');
        break;
      case 'add':
        setActiveScreen('AddObjectScreen');
        break;
      case 'search':
        setActiveScreen('SearchScreen');
        break;
      case 'profil':
        setActiveScreen('ProfilScreen');
        break;
      default:
        setActiveScreen('HomeScreen');
        break;
    }
  };

  useEffect(() => {
    if (!activeScreen) return;
    navigation.navigate(activeScreen as never);
  }, [activeScreen]);

  useEffect(() => {
    const animateIcon = (animValue: Animated.Value, isActive: boolean) => {
      const toValue = isActive ? -10 : 0;

      Animated.timing(animValue, {
        toValue,
        duration: 300,
        easing: Easing.ease,
        useNativeDriver: true,
      }).start();
    };

    animateIcon(homeTranslateYAnim, activeIcon === 'home');
    animateIcon(swipeTranslateYAnim, activeIcon === 'swipe');
    animateIcon(addObjectTranslateYAnim, activeIcon === 'add');
    animateIcon(searchTranslateYAnim, activeIcon === 'search');
    animateIcon(authTranslateYAnim, activeIcon === 'profil');

  }, [activeIcon]);

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: { backgroundColor: '#191F29', borderTopWidth: 0, height: 60 },
        tabBarIconStyle: { marginBottom: 0 },
        tabBarActiveTintColor: '#FFA451',
        tabBarShowLabel: false,
      }}
    >
      <Tab.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          headerShown: false,
          tabBarIcon: () => (
            <Animated.View
              style={{
                width: 50,
                height: 50,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{ translateY: homeTranslateYAnim }],
                marginBottom: 10,
              }}
            >
              <TouchableOpacity onPress={() => handleIconPress('home')}>
                {activeIcon === 'home' ? (
                <LinearGradientBackground colors={["#00A1EA", "#1F65E9"]}>
                  <View style={{padding: 10}}>
                    <HomeIcon size={30} color='#1C274C' />
                  </View>
                </LinearGradientBackground>
                ) : (
                  <HomeIcon size={30} color='#fff' />
                )}

                
              </TouchableOpacity>
            </Animated.View>
          ),
        }}
      />
      <Tab.Screen
        name="SwipeScreen"
        component={SwipeScreen}
        options={({ navigation }) => ({
          headerShown: true,
          header: () => <CustomHeader navigation={navigation} title="Swipe" />,
          tabBarIcon: () => (
            <Animated.View
              style={{
                width: 50,
                height: 50,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{ translateY: swipeTranslateYAnim }],
                marginBottom: 10,
              }}
            >
              <TouchableOpacity onPress={() => handleIconPress('swipe')}>
                {activeIcon === 'swipe' ? (
                  <LinearGradientBackground colors={["#00A1EA", "#1F65E9"]}>
                    <View style={{padding: 10}}>
                      <SwipeIcon size={30} color='#1C274C' />
                    </View>
                  </LinearGradientBackground>
                ) : (
                  <SwipeIcon size={30} color='#fff' />
                )}
              </TouchableOpacity>
            </Animated.View>
          ),
        })}
      />
      
      <Tab.Screen
        name="AddObjectScreen"
        component={AddObjectScreen}
        options={({ navigation }) => ({
          headerShown: true,
          header: () => <CustomHeader navigation={navigation} title="Ajout Objet" />,
          tabBarIcon: () => (
            <Animated.View
              style={{
                width: 50,
                height: 50,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{ translateY: addObjectTranslateYAnim }],
                marginBottom: 10,
              }}
            >
              <TouchableOpacity onPress={() => handleIconPress('add')}>
                {activeIcon === 'add' ? (
                  <LinearGradientBackground colors={["#00A1EA", "#1F65E9"]}>
                    <View style={{padding: 10}}>
                      <PlusCircleIcon size={30} color='#1C274C' />
                    </View>
                  </LinearGradientBackground>
                ) : (
                  <PlusCircleIcon size={30}color='#fff' />
                )}
              </TouchableOpacity>
            </Animated.View>
          ),
        })}
      />
      <Tab.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={({ navigation }) => ({
          headerShown: true,
          header: () => <CustomHeader navigation={navigation} title="Recherche" />,
          tabBarIcon: () => (
            <Animated.View
              style={{
                width: 50,
                height: 50,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{ translateY: searchTranslateYAnim }],
                marginBottom: 10,
              }}
            >
              <TouchableOpacity onPress={() => handleIconPress('search')}>
                {activeIcon === 'search' ? (
                  <LinearGradientBackground colors={["#00A1EA", "#1F65E9"]}>
                    <View style={{padding: 10}}>
                      <SearchIcon size={30} color='#1C274C' />
                    </View>
                  </LinearGradientBackground>
                ) : (
                  <SearchIcon size={30} color='#fff' />
                )}
              </TouchableOpacity>
            </Animated.View>
          ),
        })}
      />
      <Tab.Screen
        name="ProfilScreen"
        component={ProfilScreen}
        options={({ navigation }) => ({
          headerShown: true,
          header: () => <CustomHeader navigation={navigation} title="Profil" />,
          tabBarIcon: () => (
            <Animated.View
              style={{
                width: 50,
                height: 50,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{ translateY: authTranslateYAnim }],
                marginBottom: 10, // Adjust marginBottom based on translateY values
              }}
            >
              <TouchableOpacity onPress={() => handleIconPress('profil')}>
                {activeIcon === 'profil' ? (
                  <View style={{
                    borderRadius: 50,
                    borderColor: '#00A1EA',
                    borderWidth: 2,
                  }}>
                    <AvatarIcons image={currentUser?.avatar} />
                  </View>
                ) : (
                  <AvatarIcons image={currentUser?.avatar} />
                )}
              </TouchableOpacity>
            </Animated.View>
          ),
        })}
      />
    </Tab.Navigator>
  );
};

export default BottomNavBar;
