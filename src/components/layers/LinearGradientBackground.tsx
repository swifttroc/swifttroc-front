import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

interface LinearGradientBackgroundProps {
    colors: string[];
    opacity?: number;
    children: React.ReactNode;
}

const LinearGradientBackground = ({ children, colors, opacity } : LinearGradientBackgroundProps) => {
  return (
    <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        colors={colors}
        style={{opacity: opacity, padding: 0, borderRadius: 10}}>
        {children}
    </LinearGradient>
  );
};



export default LinearGradientBackground;
