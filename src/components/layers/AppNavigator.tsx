import { createStackNavigator } from '@react-navigation/stack';
import BottomNavBar from './BottomNavBar';
import WishListScreen from '../../screens/SwipeScreen/WishListScreen';
import CustomHeader from './CustomHeader';
import AddObjectScreenTwo from '../../screens/AddObjectScreen/AddObjectScreenTwo';
import RegisterScreen from '../../screens/RegisterScreen/RegisterScreen';
import TradeScreen from '../../screens/TradeScreen/TradeScreen';
import { useActiveScreen } from '../../context/ScreenContext';
import AuthScreen from '../../screens/Authscreen/AuthScreen';
import { Text, View } from 'react-native';
import DetailScreen from '../../screens/DetailScreen/DetailScreen';
import VerifyPhoneNumberScreen from '../../screens/VerifyPhoneNumberScreen/VerifyPhoneNumber';

export type RootStackParamList = {
  AuthScreen: undefined;
  RegisterScreen: undefined;
  Main: undefined;
  WishListScreen: undefined;
  AddObjectScreenTwo: undefined;
  VerifyPhoneNumberScreen: undefined;
  TradeScreen: undefined;
  DetailScreen: { object: Object };
};

const Stack = createStackNavigator<RootStackParamList>();

const AppNavigator = () => {
  const { currentUser, loader } = useActiveScreen();

  if (loader) {
    return <View style={{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    }}>
      <Text style={{
        fontSize: 20,
        fontWeight: 'bold',
      }}>Loading...</Text>
    </View>
  }
  // If there is no currentUser, immediately return the RegisterScreen stack
  if (!currentUser?._id) {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="AuthScreen"
          component={AuthScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="RegisterScreen"
          component={RegisterScreen}
          options={{
            headerShown: false,
          }}
        />

      <Stack.Screen
        name="VerifyPhoneNumberScreen"
        component={VerifyPhoneNumberScreen}
        options={{
          headerShown: false,
        }}
      />
      </Stack.Navigator>
    );
  }

  // Otherwise, return the normal stack navigator
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Main"
        component={BottomNavBar}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="WishListScreen"
        component={WishListScreen}
        options={({ navigation }) => ({
          headerShown: true,
          header: () => <CustomHeader navigation={navigation} title="Favoris" />,
        })}
      />

      <Stack.Screen
        name="AddObjectScreenTwo"
        component={AddObjectScreenTwo}
        options={({ navigation }) => ({
          headerShown: true,
          header: () => <CustomHeader navigation={navigation} title="Ajout Objet" />,
        })}
      />

      <Stack.Screen
        name="TradeScreen"
        component={TradeScreen}
        options={({ navigation }) => ({
          headerShown: false,
          // header: () => <CustomHeader navigation={navigation} title="Interface d'échange" />,
        })}
      />

      <Stack.Screen
        name="DetailScreen"
        component={DetailScreen}
        options={({ navigation }) => ({
          headerShown: true,
          header: () => <CustomHeader navigation={navigation} title="Détails" />,
        })}
      />
    </Stack.Navigator>
  );
};

export default AppNavigator;
