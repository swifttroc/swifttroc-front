import React from 'react'
import { ImageBackground } from 'react-native'

const Background = ({ children } : { children: React.ReactNode }) => {
  return (
    <ImageBackground 
      source={require('../../assets/swift_background.png')} 
      resizeMode="cover" 
      style={{flex: 1, justifyContent: 'center'}} 
    >
        {children}
    </ImageBackground>
  )
}

export default Background