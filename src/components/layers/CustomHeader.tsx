import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import { useActiveScreen } from '../../context/ScreenContext';
import ChevronLeftIcon from '../icons/ChevronLeftIcon';


const CustomHeader = ({ navigation, title }: { navigation: any; title: string }) => {

    const { setActiveScreen } = useActiveScreen();
    const goBack = () => {
        // Navigate back
        navigation.goBack();
      
        // Get the current navigation state
        const state = navigation.getState();
      
        // Find the current route's name
        const title = state.routes[state.index].name;
        console.log("GO TO", title);
        // Log the current screen title
        setActiveScreen(title);
      };

  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        paddingHorizontal: 15,
        backgroundColor: 'transparent',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 1000,
      }}
    >
      <TouchableOpacity onPress={() => goBack()}>
        <ChevronLeftIcon size={24} color='#fff' />
      </TouchableOpacity>
      <Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center', flex: 1 }}>
        {title}
      </Text>
      <View style={{ width: 24 }} />
    </View>
  );
}

export default CustomHeader;



