import React, { useState } from 'react';

import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { GestureDetector, Gesture } from 'react-native-gesture-handler';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  runOnJS,
} from 'react-native-reanimated';
import { useActiveScreen } from '../../context/ScreenContext';
import { useNavigation } from '@react-navigation/native';
import ObjectCard from './ObjectCard';


const { width } = Dimensions.get('window');

export const Swipe: React.FC<any> = ({ products }) => {

  const { addToWishlist, wishlist } = useActiveScreen();
  const navigation = useNavigation();
  const [currentIndex, setCurrentIndex] = useState(0);
  const translateX = useSharedValue(0);
  const rotate = useSharedValue(0);


  const nextProduct = () => {
    if (currentIndex < products.length - 1) {
      setCurrentIndex((prevIndex) => prevIndex + 1);
    } else {
      setCurrentIndex(0);
    }
  };

  const panGesture = Gesture.Pan()
  .onUpdate((event) => {
    translateX.value = event.translationX;
    rotate.value = event.translationX / 20; // Adjust the divisor to control the rotation
  })
  .onEnd(() => {
    if (translateX.value < -width * 0.25) {
      translateX.value = withSpring(-width, {
        duration: 200,
        dampingRatio: 4.5,
        stiffness: 100,
      }, () => {
        runOnJS(nextProduct)();
        translateX.value = 0;
        rotate.value = 0;
      });
    } else {
      console.log('Add to wishlist');

      // Extract the item ID and pass it to runOnJS
      const item = products[currentIndex];

      // Use runOnJS to call the addToWishlist function with the item ID
      runOnJS(addToWishlist)(item);
      translateX.value = withSpring(width, {
        duration: 200,
        dampingRatio: 4.5,
        stiffness: 100,
      }, () => {
        runOnJS(nextProduct)();
        translateX.value = 0;
        rotate.value = 0;
      });
    }
  });


  const animatedStyle = useAnimatedStyle(() => ({
    transform: [
      { translateX: translateX.value },
      { rotate: `${rotate.value}deg` }, // Apply rotation
    ],
  }));

  const currentProduct = products[currentIndex];
  const nextIndex = (currentIndex + 1) % products.length;
  const nextProductData = products[nextIndex];
  

  return (
    <View style={styles.container}>
      {/* Next Product Card */}
      <View style={styles.cardContainer}>
        <Animated.View style={[styles.card, styles.nextCard]}>
          <ObjectCard object={nextProductData} size='large' />
        </Animated.View>
      </View>

      {/* Current Product Card */}
      <GestureDetector gesture={panGesture}>
        <Animated.View style={[styles.card, animatedStyle]}>
          <ObjectCard object={currentProduct} size='large' />
        </Animated.View>
      </GestureDetector>

      <TouchableOpacity
        onPress={() => navigation.navigate('WishListScreen' as never)}
        style={{
          borderColor: 'white',
          borderWidth: 1,
          position: 'absolute',
          width: 140,
          bottom: 40,
          right: -10,
          backgroundColor: '#267be6',
          padding: 10,
          borderRadius: 20,
          elevation: 10,
        }}>
        <Text style={{
          fontSize: 16,
          fontWeight: 'bold',
          color: 'white',
          textAlign: 'center',
        }}> Favoris </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 10,
  },
  cardContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
    marginLeft: width * 0.1,
    marginTop: 40,
  },
  card: {
    width: width * 0.8,
    height: "72%",
    alignSelf: 'center',
    borderRadius: 10,
    alignItems: 'center',
  },
  nextCard: {
    zIndex: -1,
  },
  image: {
    marginTop: 10,
    width: '100%',
    height: "74%",
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  category : {
    fontSize: 16,
    color: 'white',
    opacity: 0.6,
  },
  description: {
    fontSize: 16,
    color: 'gray',
    marginVertical: 10,
    textAlign: 'center',
  },
  price: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'green',
  },
});
