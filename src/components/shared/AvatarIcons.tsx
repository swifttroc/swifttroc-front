import { View, Text, Image } from 'react-native'
import React from 'react'

const AvatarIcons = ({ size, image }: { size?: number, image: string }) => {
  return (
    <View style={{
        borderColor: 'blue',
        borderRadius: 50,
    }}>
        <Image
            style={{
                width: size || 40,
                height: size || 40,
                borderRadius: 50,
            }}
            source={{ uri: image}}
        />
    </View>
  )
}

export default AvatarIcons