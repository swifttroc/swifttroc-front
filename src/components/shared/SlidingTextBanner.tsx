import React, { useEffect, useRef } from 'react';
import { View, Text, Animated, Dimensions, StyleSheet } from 'react-native';

const SlidingTextBanner: React.FC = () => {
  const slideAnim = useRef(new Animated.Value(0)).current;
  const { width } = Dimensions.get('window');

  useEffect(() => {
    // Infinite loop of sliding animation
    Animated.loop(
      Animated.sequence([
        Animated.timing(slideAnim, {
          toValue: width,  // Slide to the right
          duration: 4000,  // Adjust duration as needed
          useNativeDriver: true,
        }),
        Animated.timing(slideAnim, {
          toValue: -width, // Slide back to the left
          duration: 0,     // Instantly reset to the left
          useNativeDriver: true,
        }),
      ]),
    ).start();
  }, [slideAnim, width]);

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.animatedTextContainer, { transform: [{ translateX: slideAnim }] }]}>
        <Text style={styles.slidingText}>Nouveauté de l'application</Text>
      </Animated.View>
    </View>
  );
};

export default SlidingTextBanner;

const styles = StyleSheet.create({
  container: {
    height: 50,  // Height of the banner
    overflow: 'hidden',  // Hide the text when it slides out of the view
    backgroundColor: 'rgba(255, 255, 255, 0.6)',  // Semi-transparent white background
    justifyContent: 'center',  // Center vertically
  },
  animatedTextContainer: {
    position: 'absolute',
  },
  slidingText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333',
  },
});
