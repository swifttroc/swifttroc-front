import React from 'react';
import { View, Text, Image } from 'react-native';
import { BlurView } from '@react-native-community/blur';
import { capitalizeFirstLetter } from '../../utils/function';
import { HeartFillIcon, HeartOutlineIcon } from '../icons/HeartIcons';
import { useActiveScreen } from '../../context/ScreenContext';

type Object = {
  _id: string;
  title: string;
  description: string;
  categories: string[];
  images: string[];
};

const ObjectCard = ({ object, size }:
  { object: Object, size?: string }) => {

    const { wishlist } = useActiveScreen();

    const inWishList = (id: string) => {
      const index = wishlist.findIndex((item: any) => item._id === id);
      return index===-1 ? false : true;
    };

  return (
    <View style={{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
      alignSelf: 'center',
      borderRadius: 10,
      paddingVertical: size === 'small' ? 20 : 6,
      elevation: 2,
      overflow: 'hidden',  // Prevents the blur from leaking outside the borders
    }}>
      <BlurView
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          backgroundColor: 'rgba(50,64,125,0.5)',
        }}
        blurType="light"  // "dark" or "regular" or "light"
        blurAmount={4} // 1 to 10
        reducedTransparencyFallbackColor="#32407d"
      />
      
      <Text style={{
        fontSize: size === 'small' ? 14 : 20,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center',
        paddingHorizontal: 4,
        zIndex: 1,  // Ensure the text is above the blur
      }}
        numberOfLines={2}
        ellipsizeMode='tail'
      >
        {capitalizeFirstLetter(object.title)}
      </Text>

      <Text style={{
        fontSize: size === 'small' ? 12 : 16,
        color: 'white',
        opacity: 0.6,
        zIndex: 1,  // Ensure the text is above the blur
      }}>
        {capitalizeFirstLetter(object.categories[0])}
      </Text>

      <View style={{
        width: '100%',
        height: '74%',
        alignSelf: 'center',
        borderRadius: 4,
        alignItems: 'center',
        overflow: 'hidden',
        zIndex: 1,  // Ensure the image is above the blur
      }}>
        <Image
          source={{ uri: object.images[0] }}
          style={{
            marginTop: 10,
            width: '100%',
            height: '100%',
            borderRadius: 4,
          }}
        />

        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 6,
          }}>
          {inWishList(object._id) ?
            <HeartFillIcon size={size === 'small' ? 28 : 40} />
            : <HeartOutlineIcon size={size === 'small' ? 28 : 40} />}
        </View>
      </View>

      <Text style={{
        fontSize: size === 'small' ? 12 : 16,
        color: 'white',
        marginVertical: 10,
        textAlign: 'center',
        opacity: 0.6,
        paddingHorizontal: 4,
        zIndex: 1,  // Ensure the text is above the blur
      }}
        numberOfLines={2}
        ellipsizeMode='tail'
      >
        {capitalizeFirstLetter(object.description)}
      </Text>
    </View>
  )
};

export default ObjectCard;
