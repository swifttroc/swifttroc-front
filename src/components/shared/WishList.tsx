import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import ObjectCard from './ObjectCard'
import { useActiveScreen } from '../../context/ScreenContext';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { RootStackParamList } from '../layers/AppNavigator';

const WishList = () => {
    const { wishlist } = useActiveScreen();
    const navigation = useNavigation<NavigationProp<RootStackParamList>>();


  return (
    <View style={{
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        marginTop: 20,
    }}>
        {wishlist.length > 0 ? wishlist.map((object: any) => {
            return (
            
            <TouchableOpacity onPress={() => navigation.navigate('DetailScreen', { object })} key={object._id} style={{
                width: '45%',
                margin: 5,
                height: 260,
            }}>
                <ObjectCard object={object} size='small' />
            </TouchableOpacity>
            )
        }) : <Text style={{
            fontSize: 16,
            fontWeight: 'bold',
            textAlign: 'center',
            marginTop: 20,
        }}>Pas d'objets en favoris</Text>}

    </View>
  )
}

export default WishList