import styled from 'styled-components/native';
import { colors } from '../../colors';

const { primary, dark } = colors;


export const Title = styled.Text`
    color: ${primary};
    font-size: 30px;
    font-weight: bold;
    text-align: center;
    margin: 20px;
`;
