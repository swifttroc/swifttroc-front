import { View, Text, TouchableOpacity, Modal } from 'react-native'
import React, { useCallback, useEffect, useState } from 'react'
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { TradesAPI } from '../../API/TradesAPI';
import AvatarIcons from './AvatarIcons';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { useActiveScreen } from '../../context/ScreenContext';
import Toast from 'react-native-toast-message';

const TradesList = () => {

    const { currentUser } = useActiveScreen();
    const [trades, setTrades] = useState([]);
    const [filteredTrades, setFilteredTrades] = useState([]);
    const [type, setType] = useState('received');
    const [isPopupVisible, setIsPopupVisible] = useState(false);
    interface Trade {
        proposerAcceptedTrade: boolean;
        recipientAcceptedTrade: boolean;
        _id: string;
        proposer: {
            name: string;
            lastName: string;
            phone: string;
            email: string;
            avatar: string;
            pseudo: string;

        };

        recipient: {
            _id: string;
            name: string;
            lastName: string;
            phone: string;
            email: string;
            avatar: string;
            pseudo: string;
        };
        status: string;
        createdAt: string;
    }

    const [selectedTrade, setSelectedTrade] = useState<Trade | null>(null);
    const navigation: any = useNavigation();

    useFocusEffect(
        useCallback(() => {
            getTrades();
        }, [])
    );

    useEffect(() => {
        // filter trades by status
        if (type === 'received') {
            setFilteredTrades(trades?.filter((trade: any) => trade.recipient?._id === currentUser?._id));
        } else {
            setFilteredTrades(trades?.filter((trade: any) => trade.proposer?._id === currentUser?._id));
        }
    }, [type, trades]);

    const getTrades = async () => {
        try {
            const response: any = await TradesAPI.GET();
            setTrades(response.data);
        } catch (error) {
            Toast.show({
                type: 'error',
                position: 'top',
                text1: 'Erreur',
                text2: 'Une erreur est survenue lors de la récupération des trocs.'
            });
        }
    };

    const goToTrade = (trade: any) => {
        navigation.navigate('TradeScreen', { trade, type });
    };

    const openPopup = (trade: any) => {
        setSelectedTrade(trade);
        setIsPopupVisible(true);
    };

    const closePopup = () => {
        setIsPopupVisible(false);
        setSelectedTrade(null);
    };

    const getDelay = (date: string) => {
        const now = new Date();
        const created = new Date(date);
        const diff = now.getTime() - created.getTime();
        const seconds = Math.floor(diff / 1000);
        const minutes = Math.floor(seconds / 60);
        const hours = Math.floor(minutes / 60);
        const days = Math.floor(hours / 24);
        const months = Math.floor(days / 30);
        const years = Math.floor(months / 12);
        if (years > 0) return years + ' ans';
        if (months > 0) return months + ' mois';
        if (days > 0) return days + ' jours';
        if (hours > 0) return hours + ' heures';
        if (minutes > 0) return minutes + ' minutes';
        if (seconds > 0) return seconds + ' secondes';

        return '';
    };

    if (!trades || trades?.length === 0) {
        return <Text style={{ color: "#fff", fontSize: 18, textAlign: 'center' }}>Chargement...</Text>;
    }

    // Fonction pour mettre à jour le statut du trade
    const updateTradeStatus = async (tradeId: string, fieldToUpdate: string) => {
        try {
            const response = await TradesAPI.PUT_BOOLEAN(tradeId, fieldToUpdate, true);


            if (response.status === 200) {
                Toast.show({
                    type: 'success',
                    position: 'top',
                    text1: 'Succès',
                    text2: 'Le statut de la transaction a été mis à jour avec succès.'
                });
                getTrades(); // Pour rafraîchir la liste des trades après la mise à jour
            } else {
                Toast.show({
                    type: 'error',
                    position: 'top',
                    text1: 'Erreur',
                    text2: 'Échec de la mise à jour du statut.'
                });
            }
        } catch (error) {
            console.error('Erreur lors de la mise à jour:', error);
            Toast.show({
                type: 'error',
                position: 'top',
                text1: 'Erreur',
                text2: 'Une erreur est survenue lors de la mise à jour.'
            });
        }
    };




    return (
        <ScrollView contentContainerStyle={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            width: '100%',
            gap: 15,
        }}>
            <View style={{
                display: 'flex',
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 10,
                gap: 10,
            }}>
                <TouchableOpacity onPress={() => setType('received')}>
                    <View style={{ backgroundColor: type === 'received' ? 'rgba(255, 255, 255, 0.6)' : 'transparent', padding: 5, borderRadius: 8 }}>
                        <Text style={{ fontWeight: 'bold', color: "#fff", fontSize: 16, textAlign: 'center' }}>Demandes Reçues</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setType('sent')}>
                    <View style={{ backgroundColor: type === 'sent' ? 'rgba(0, 255, 255, 0.6)' : 'transparent', padding: 5, borderRadius: 8 }}>
                        <Text style={{ fontWeight: 'bold', color: "#fff", fontSize: 16, textAlign: 'center' }}>Demandes Envoyées</Text>
                    </View>
                </TouchableOpacity>
            </View>

            {filteredTrades.length === 0 && type === 'received' && <Text style={{ color: "#fff", fontSize: 18, textAlign: 'center' }}>Aucune demande reçue</Text>}
            {filteredTrades.length === 0 && type === 'sent' && <Text style={{ color: "#fff", fontSize: 18, textAlign: 'center' }}>Aucune demande envoyée</Text>}

            {filteredTrades?.map((trade: any) => (
                <View key={trade._id} style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    gap: 10,
                    paddingHorizontal: 10,
                }}>
                    {/* Entire trade row is clickable to go to TradeScreen */}
                    <TouchableOpacity onPress={() => goToTrade(trade)} style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        gap: 10,
                        flex: 1,
                    }}>
                        <AvatarIcons size={50} image={type === 'received' ? trade.proposer?.avatar : trade.recipient?.avatar} />
                        <Text numberOfLines={1} style={{ fontWeight: 'bold', color: "#fff", fontSize: 18 }}>{type === 'received' ? trade.proposer?.pseudo : trade.recipient?.pseudo}</Text>
                    </TouchableOpacity>

                    {/* Status button */}
                    <View style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}>
                        {/* The button that opens the popup only for accepted trades */}
                        <TouchableOpacity onPress={() => trade.status === 'accepted' && openPopup(trade)}>
                            <View style={{
                                backgroundColor: trade.status === 'pending' ? '#FFA500' : trade.status === 'accepted' ? '#00b900' : '#FF0000',
                                padding: 5,
                                paddingHorizontal: 15,
                                borderRadius: 5,
                            }}>
                                <Text style={{ color: "#fff", fontSize: 16, textAlign: 'center' }}>{trade.status}</Text>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ color: "#fff", fontSize: 16 }}>Il y a {getDelay(trade.createdAt)}</Text>
                    </View>
                </View>
            ))}

            {/* Popup for accepted trades */}
            <Modal
                transparent={true}
                visible={isPopupVisible}
                onRequestClose={closePopup}
            >
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                }}>
                    <View style={{
                        width: 300,
                        padding: 20,
                        backgroundColor: 'white',
                        borderRadius: 10,
                        alignItems: 'center',
                    }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#000' }}>Détails de la demande</Text>
                        {/* Vérification que la demande est reçue */}
                        {selectedTrade && type === 'received' && (
                            <>
                                <Text style={{ marginVertical: 10, color: '#000' }}>ID de la proposition: {selectedTrade._id}</Text>
                                <Text style={{ marginVertical: 10, color: '#000' }}>Nom: {selectedTrade.proposer?.name ?? 'Inconnu'}</Text>
                                <Text style={{ marginVertical: 10, color: '#000' }}>Prénom: {selectedTrade.proposer?.lastName}</Text>
                                <Text style={{ marginVertical: 10, color: '#000' }}>Téléphone: {selectedTrade.proposer?.phone}</Text>
                                <Text style={{ marginBottom: 10, color: '#000' }}>Email: {selectedTrade.proposer?.email}</Text>
                                <Text style={{ marginBottom: 10, color: '#000' }}>Vous avez accepté la proposition de troc de {selectedTrade.proposer?.name} ! convenez d'un RDV avec {selectedTrade.proposer?.name} pour vous échanger vos objets</Text>

                                {selectedTrade?.proposerAcceptedTrade === false ? (
                                    <TouchableOpacity
                                        onPress={() => updateTradeStatus(selectedTrade._id, 'proposerAcceptedTrade')}
                                        style={{
                                            backgroundColor: '#00f',
                                            padding: 10,
                                            borderRadius: 5,
                                            marginTop: 10,
                                        }}
                                    >
                                        <Text style={{ color: '#fff' }}>Déclarer la transaction comme faite !!</Text>
                                    </TouchableOpacity>
                                ) : (
                                    <Text style={{ marginBottom: 10, color: 'green' }}>Transaction validée ! Vous avez effectué l'échange avec {selectedTrade.proposer?.name} {selectedTrade.proposer?.lastName}.
                                    </Text>
                                )}
                            </>
                        )}
                        {/* Ajout pour les demandes envoyées */}
                        {selectedTrade && type === 'sent' && (
                            <>
                                <Text style={{ marginVertical: 10, color: '#000' }}>ID de la proposition: {selectedTrade._id}</Text>
                                <Text style={{ marginVertical: 10, color: '#000' }}>Nom: {selectedTrade.recipient?.name}</Text>
                                <Text style={{ marginVertical: 10, color: '#000' }}>Prénom: {selectedTrade.recipient?.lastName}</Text>
                                <Text style={{ marginVertical: 10, color: '#000' }}>Téléphone: {selectedTrade.recipient?.phone}</Text>
                                <Text style={{ marginVertical: 10, color: '#000' }}>Email: {selectedTrade.recipient?.email}</Text>

                                <Text style={{ marginBottom: 10, color: '#000' }} onPress={() => console.log('Validation RECIPIENT', selectedTrade?.recipientAcceptedTrade)}>validation du recipient ? {selectedTrade?.recipientAcceptedTrade}</Text>
                                <Text style={{ marginBottom: 10, color: '#000' }}>Votre proposition de troc a été acceptée ! Convenez d'un RDV avec {selectedTrade.recipient?.name} pour vous échanger vos objets</Text>
                                {selectedTrade?.recipientAcceptedTrade === false ? (
                                    <TouchableOpacity
                                        onPress={() => updateTradeStatus(selectedTrade._id, 'recipientAcceptedTrade')}
                                        style={{
                                            backgroundColor: '#00f',
                                            padding: 10,
                                            borderRadius: 5,
                                            marginTop: 10,
                                        }}
                                    >
                                        <Text style={{ color: '#fff' }}>Déclarer la transaction comme faite</Text>
                                    </TouchableOpacity>
                                ) : (
                                    <Text style={{ marginBottom: 10, color: 'green' }}>Transaction validée ! Vous avez effectué l'échange avec {selectedTrade?.recipient.name} {selectedTrade?.recipient.lastName}.</Text>
                                )}


                            </>
                        )}
                        <TouchableOpacity onPress={closePopup} style={{
                            backgroundColor: '#f00',
                            padding: 10,
                            borderRadius: 5,
                        }}>
                            <Text style={{ color: '#fff' }}>Fermer</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>


        </ScrollView>
    )
}

export default TradesList;
