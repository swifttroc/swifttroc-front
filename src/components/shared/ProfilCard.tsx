import { View, Text, TouchableOpacity, Image } from 'react-native';
import React from 'react';
import { useActiveScreen } from '../../context/ScreenContext';
import { capitalizeFirstLetter } from '../../utils/function';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PinIcon from '../icons/PinIcon';
import LogoutIcon from '../icons/LogoutIcon';
import SettingIcon from '../icons/SettingIcon';

interface ProfilCardProps {
    activeMenu: string;
    setActiveMenu: (menu: string) => void;
    objectsLength?: number;
    tradesLength?: number;
    onEditProfile: () => void;
}

const ProfilCard = ({ activeMenu, setActiveMenu, objectsLength = 0, tradesLength = 0, onEditProfile }: ProfilCardProps) => {

    const { currentUser, setCurrentUser, wishlist } = useActiveScreen();
    const name_lastname = `${capitalizeFirstLetter(currentUser?.name)} ${capitalizeFirstLetter(currentUser?.lastName)}`;
    const menus = ["Mes objets", "Favoris", "Demandes"];

    const handleLogout = () => {
        AsyncStorage.removeItem('token');
        setCurrentUser(null);
    };

   
    return (
        <View style={{
            width: '100%',
            height: 300,
            borderRadius: 20,
            borderColor: '#979797',
            borderWidth: 1,
            padding: 15,
        }}>
            <TouchableOpacity style={{
                alignSelf: 'center',
            }}>
             <Image
                source={{ uri: currentUser?.avatar || 'https://via.placeholder.com/150' }}
                style={{
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                }}
            />
            </TouchableOpacity>
            <Text 
            numberOfLines={2}
            style={{
                color: '#fff',
                textAlign: 'center',
                fontSize: name_lastname.length > 25 ? 20 : 26,
                fontWeight: 'bold',
                marginTop: name_lastname.length > 25 ? 5 : 10,
            }}>
                {name_lastname}
            </Text>
        <View style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            gap: 8,
        }}>
            <PinIcon size={12} />
            <Text
            numberOfLines={1}
            ellipsizeMode='tail'
            style={{
                color: '#e2e2e2',
                textAlign: 'center',
                fontSize: 16,
                marginTop: 4,
                fontStyle: 'italic',
            }}>{((currentUser?.location?.road || '') + ", " + (currentUser?.location?.country || '')) || 'Non renseigné'}
            </Text>
        </View>


            <View style={{
                width: '100%',
                height: 70,
                marginTop: 20,
                backgroundColor: '#94d4f2',
                borderColor: '#fff',
                borderWidth: 4,
                borderRadius: 20,
            }}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    padding: 10,
                }}>
                    {menus.map((menu, index) => (
                        <TouchableOpacity onPress={() => setActiveMenu(menu)} key={index} style={{
                            borderRadius: 10,
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}>
                            <Text style={{
                                color: '#000',
                                fontSize: 16,
                                fontWeight: activeMenu === menu ? 'bold' : 'normal',
                            }}>
                                {menu === "Mes objets" && objectsLength}
                                {menu === "Favoris" && wishlist.length}
                                {menu === "Demandes" && tradesLength}
                            </Text>
                            <Text style={{
                                color: '#000',
                                fontSize: 16,
                                fontWeight: activeMenu === menu ? 'bold' : 'normal',
                            }}>{menu}
                            </Text>
                        </TouchableOpacity>
                    ))}
                </View>
                
                {/* Ligne avec Déconnexion et Modifier le profil */}
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 14,
                }}>
                    <TouchableOpacity style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        gap: 6,
                    }} onPress={handleLogout}>
                        <LogoutIcon size={26} />
                        <Text style={{
                            color: '#fff',
                            fontSize: 16,
                            fontWeight: 'bold',
                        }}> Déconnexion
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        gap: 6,
                    }} onPress={onEditProfile}>
                        <SettingIcon size={26} />
                        <Text style={{
                            color: '#fff',
                            fontSize: 16,
                            fontWeight: 'bold',
                        }}> Modifier le profil
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default ProfilCard;
