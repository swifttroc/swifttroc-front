import React from 'react'
import { StyleSheet, View } from 'react-native'
import { BlurView } from '@react-native-community/blur';

interface BlurCardProps {
    width: number;
    height: number;
    children: React.ReactNode;
    
} 

const BlurCard = ({ width, height, children }: BlurCardProps) => {
  return (
    <View style={{width: width, height: height}}>
        <BlurView
          style={styles.absolute}
          blurType="light" // You can change this to 'dark', 'light', 'xlight', 'prominent', 'regular'
          blurAmount={10} // Adjust the blur intensity
        >
          {children}
        </BlurView>
    </View>
  );
};

const styles = StyleSheet.create({
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  
});


export default BlurCard