import { View, Text, TextInput } from 'react-native'
import React from 'react'

type TextFieldProps = {
    placeholder: string,
    value: string,
    onChangeText: (text: string) => void,
    type?: string,
}

const TextField = ({ placeholder, value, onChangeText, type }: TextFieldProps) => {
  return (
    <TextInput 
        style={{
            borderColor: 'white',
            borderWidth: 1.5,
            borderRadius: 18,
            margin: 20,
            padding: 10,
            width: '90%',
            alignSelf: 'center',
            color: 'white',
            textAlign: 'center',
        }}
        secureTextEntry={type === 'password'}
        placeholder={placeholder}
        placeholderTextColor='white'
        value={value}
        onChangeText={onChangeText}
    />
  )
}

export default TextField