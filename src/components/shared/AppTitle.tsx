import React from 'react'
import * as Styling from './AppTitle-styling'

const AppTitle = () => {
  return (
    <Styling.Title>
        SwiftTroc
    </Styling.Title>
  )
}

export default AppTitle