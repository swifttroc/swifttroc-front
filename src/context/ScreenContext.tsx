// ActiveScreenContext.tsx
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useState, useContext, ReactNode, useEffect } from 'react';
import { Object } from '../types/index.';
import { getUser } from '../utils/function';
import Toast from 'react-native-toast-message';

interface ActiveScreenContextProps {
  currentUser: any;
  setCurrentUser: (user: any) => void;
  loader: boolean;
  setLoader: (loader: boolean) => void;
  activeScreen: string;
  setActiveScreen: (screen: string) => void;
  wishlist: string[];
  setWishlist: (wishlist: string[]) => void;
  addToWishlist: (itemId: string) => void;
  removeFromWishlist: (itemId: string) => void;
  objectToAdd: any;
  setObjectToAdd: (object: any) => void;
}

const ActiveScreenContext = createContext<ActiveScreenContextProps | undefined>(undefined);

export const ActiveScreenProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [currentUser, setCurrentUser] = useState<any>(null);
  const [loader, setLoader] = useState<boolean>(false);
  const [activeScreen, setActiveScreen] = useState<string>('HomeScreen');
  const [wishlist, setWishlist] = useState<string[]>([]);
  const [objectToAdd, setObjectToAdd] = useState<any>({
    title: '',
    description: '',
    categories: [],
    images: [],
  });

  useEffect(() => {
    const getWishlist = async () => {
      try {
        const wishlistStorage = await AsyncStorage.getItem('wishlist');
        const wishlistArray = wishlistStorage ? JSON.parse(wishlistStorage) : [];
        setWishlist(wishlistArray);
      } catch (error) {
        console.error('Error getting wishlist:', error);
      }
    };
    
    const emptyWishList = async () => {
      await AsyncStorage.removeItem('wishlist');
    };

    authUser();
    getWishlist();
    // emptyWishList();
  }, []);

  const authUser = async () => {
    setLoader(true);
    const user = await getUser();
    if(!user) {
      setLoader(false);
      return null;
    }

    setCurrentUser(user);
    setLoader(false);

    return user;
  }

  const addToWishlist = async (item: any) => {
    try {
      const wishlistStorage = await AsyncStorage.getItem('wishlist');
      let wishlistArray = wishlistStorage ? JSON.parse(wishlistStorage) : [];
  
      // Check if the item is already in the wishlist to avoid duplicates
      const wish = wishlistArray.find((i: any) => i._id === item._id);
      if (!wish) {
        wishlistArray.push(item);
        await AsyncStorage.setItem('wishlist', JSON.stringify(wishlistArray));
        Toast.show({
          type: 'success',
          position: 'top',
          text1: 'Succès',
          text2: 'Objet ajouté à vos favoris',
          visibilityTime: 2000,
        });
      } else {
        console.log('Item already in wishlist');
      }
      setWishlist(wishlistArray);
    } catch (error) {
      console.error('Error adding item to wishlist:', error);
    }
  };

  const removeFromWishlist = async (itemId: string) => {
    try {
      const wishlistStorage = await AsyncStorage.getItem('wishlist');
      let wishlistArray = wishlistStorage ? JSON.parse(wishlistStorage) : [];
  
      // Remove the item from the wishlist
      wishlistArray = wishlistArray.filter((item: any) => item._id !== itemId);
      await AsyncStorage.setItem('wishlist', JSON.stringify(wishlistArray));
      setWishlist(wishlistArray);
      Toast.show({
        type: 'success',
        position: 'top',
        text1: 'Succès',
        text2: 'Objet retiré de vos favoris',
        visibilityTime: 2000,
      });
    } catch (error) {
      console.error('Error removing item from wishlist:', error);
    }
  };

  return (
    <ActiveScreenContext.Provider 
      value={{
        currentUser,
        setCurrentUser,
        loader,
        setLoader,
        activeScreen, 
        setActiveScreen, 
        wishlist,
        setWishlist,
        addToWishlist,
        removeFromWishlist,
        objectToAdd,
        setObjectToAdd,
      }}>
      {children}
    </ActiveScreenContext.Provider>
  );
};

export const useActiveScreen = (): ActiveScreenContextProps => {
  const context = useContext(ActiveScreenContext);
  if (!context) {
    throw new Error('useActiveScreen must be used within an ActiveScreenProvider');
  }
  return context;
};
