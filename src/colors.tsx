export const colors = {
    primary: '#4B4CED',
    light: null,
    dark: '#0c0101',
    specialTheme: null,
     gray: '#bdc3c7',
  accent: '#e74c3c'
};