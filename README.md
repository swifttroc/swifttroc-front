# Getting Started

## Step 1: Lancer le projet

```bash
# using npm
npm run start

```




## Liste des packages installés et leurs versions :

styled-component : 6.1.11 // installed with --legacy-peer-deps
react-native-gesture-handler: 2.16.2
react-native-safe-area-context : 4.10.3
react-navigation/native-stack : 6.9.26
react-navigation/stack : 6.3.29
react: 18.2.0,
react-native: 0.74.1
npm install @react-navigation/native
npm install @react-navigation/native @react-navigation/native-stack --legacy-peer-deps


## CONERNANT LE GRADLE : 
La version 8.6 pose problème. il faut downgrade vers la version 8.5 : 

distributionBase=GRADLE_USER_HOME
distributionPath=wrapper/dists
distributionUrl=https\://services.gradle.org/distributions/gradle-8.5-all.zip
networkTimeout=10000
validateDistributionUrl=true
zipStoreBase=GRADLE_USER_HOME
zipStorePath=wrapper/dists
