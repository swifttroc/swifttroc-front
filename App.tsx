import React from 'react';
import Router from './src/routes/router';
import styles from './src/styles';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import Toast from 'react-native-toast-message';

const App: React.FC = () =>  {
  return (
    <GestureHandlerRootView style={styles.root}>
        <Router />
        <Toast />
    </GestureHandlerRootView>
  );
}

export default App;